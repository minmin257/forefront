<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('abouts', function (Blueprint $table) {
            $table->bigIncrements('id'); //1 
            $table->foreignId('sub_id')
                ->constrained('subs')
                ->onDelete('cascade');
            $table->longText('content')->nullable(); //history為空
            $table->longText('en_content')->nullable();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_profiles');
    }

}
