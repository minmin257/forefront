<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('sub_id')
                ->constrained('subs')
                ->onDelete('cascade');
            $table->string('title');
            $table->string('logo');
            $table->string('tel');
            $table->string('fax');
            $table->string('email')->comment('公司信箱');
            $table->text('map');
            $table->string('src');
            $table->longText('content');
            $table->longText('en_content');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
