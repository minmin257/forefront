<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('sub_id')
                ->constrained('subs')
                ->onDelete('cascade');
            $table->string('year');
            $table->boolean('state')->comment('啟用狀態(0關閉/1顯示)')->default(1);
            $table->string('delete')->default(0);
            $table->string('content');
            $table->string('en_content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
