<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sitename')->comment('網站名稱');
            $table->string('meta_description')->comment('meta_description')->nullable();
            $table->string('meta_keyword')->comment('meta_keyword')->nullable();
            $table->boolean('maintain')->default(0)->comment('0關閉/1維護');
            $table->string('message')->comment('維護訊息');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }
}
