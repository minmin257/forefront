<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('src')->nullable();
            $table->string('en_src')->nullable();
            $table->string('link')->nullable();
            $table->string('en_link')->nullable();
            $table->integer('sort')->comment('順序')->default(0);
            $table->boolean('state')->comment('啟用狀態(0關閉/1顯示)')->default(1);
            $table->integer('en_sort')->comment('順序')->default(0);
            $table->boolean('en_state')->comment('啟用狀態(0關閉/1顯示)')->default(1);
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
