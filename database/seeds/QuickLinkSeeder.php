<?php

use Illuminate\Database\Seeder;
use App\QuickLink;

class QuickLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        QuickLink::create([
        	'name'=>'藥品原料',
        	'en_name'=>'API',
        	'link'=>'https://forefront.jhong-demo.com.tw/Products/3',
            'en_link'=>'https://forefront.jhong-demo.com.tw/Products/3',
        	'src'=>'/images/p-01.jpg',
            'en_src'=>'/images/p-01.jpg',
        ]);

        QuickLink::create([
        	'name'=>'飼料原料',
        	'en_name'=>'Feed',
        	'link'=>'https://forefront.jhong-demo.com.tw/Products/4',
            'en_link'=>'https://forefront.jhong-demo.com.tw/Products/4',
        	'src'=>'/images/p-02.jpg',
            'en_src'=>'/images/p-02.jpg',
        ]);

        QuickLink::create([
        	'name'=>'飼料添加劑',
        	'en_name'=>'Feed Additives',
        	'link'=>'https://forefront.jhong-demo.com.tw/Products/5',
            'en_link'=>'https://forefront.jhong-demo.com.tw/Products/5',
        	'src'=>'/images/p-03.jpg',
            'en_src'=>'/images/p-03.jpg',
        ]);


    }
}
