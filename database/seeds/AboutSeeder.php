<?php

use Illuminate\Database\Seeder;
use App\About;
class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        About::create([
          'sub_id'=>'1',
        	'content'=>'<div class="row mb-3">
              <div class="col-12 col-lg-8">
                <p>
                  丞泰企業有限公司創立於1974年11月22日，資本額新台幣4500萬，以誠信為立業基礎，以穩健為經營方針，迄今已逾45年。<br>
                  丞泰企業有限公司主要營業項目為：藥品原料、飼料添加劑等相關產品之進口與行銷業務。藥品部分有--西藥原料、維生素抗生素、動物用藥品原料、藥品輔料、中間體原料等，飼料添加劑部分有--維生素、乳製品、氨基酸等。在創新、合理、透明的管理原則下業務蒸蒸日上，深獲國外供應商的支持與國內顧客的信賴，更獲得業界好評。
                </p>
              </div>
              <div class="col-12 col-sm-6 col-lg-4">
                <img src="/images/about-01.jpg" class="w-100 mb-3" alt="">
              </div>
              <div class="col-12 col-sm-6 col-lg-4">
                <img src="/images/about-02.jpg" class="w-100 mb-3" alt="">
              </div>
              <div class="col-12 col-lg-8">
                <p>
                  丞泰企業有限公司逾45年的誠信經營，已經奠定穩固的發展基礎，健全的財務結構及與顧客建立的長久夥伴關係，是促進業務成長的利基，所代理國外產品之供應商更是龐大的支持系統，造就了丞泰企業有限公司不斷的進步與發展。<br>
                  丞泰企業有限公司專精於藥品、飼料的進口與行銷，專業的行銷人員具有豐富的藥學、畜牧方面的專業知識與實務經驗，能迅速提供客戶各式問題的解決方案。為協助客戶的生產達到最佳化，丞泰企業有限公司所進口的產品均經過國際標準的實驗室檢驗，以確保每項產品皆是高品質與高效能之產品，並確實符合產品規格與生物可利用效率。
                </p>
              </div>
              <div class="col-12">
                <p>
                  丞泰企業有限公司的使命是：關注我們的顧客，提供超越顧客期望的產品與服務，建立與顧客長久的夥伴關係。<br>
                  丞泰企業有限公司的願景是：成為台灣藥品原料、飼料添加劑的首屈供應者。<br>
                  展望未來，丞泰企業有限公司將擴大經營規模，延聘優秀專業人才，開創新局，其成果將與所有員工共享。
                </p>
              </div>
            </div>',

        	'en_content'=>'<div class="row mb-3">
              <div class="col-12 col-lg-8">
                <p>
                  Forefront Enterprise Co., Ltd. was founded in 1974. <br>
                  With more than 45 years professional experience engaged in supplying quality materials Forefront has laid a solid foundation for development. We have been a leading agency in Active Pharmaceutical Ingredients (APIs) and Feed additives.
                </p>
                  Our major business includes import and marketing of below fields.
                <ol class="pl-3">
                  <li>Pharmaceutical Ingredients (APIs): Human and Animal API which are vitamin and antibiotics…etc.</li>
                  <li>Feed additives and other related products: Vitamins, dairy products and amino acids.</li>
                </ol>
              </div>
              <div class="col-12 col-sm-6 col-lg-4">
                <img src="/images/about-01.jpg" class="w-100 mb-3" alt="">
              </div>
              <div class="col-12 col-sm-6 col-lg-4">
                <img src="/images/about-02.jpg" class="w-100 mb-3" alt="">
              </div>
              <div class="col-12 col-lg-8">
                <p>
                  Forefront Enterprise Co., Ltd. owns professional marketing personnel possessed of well knowledge and practical experience in the fields of pharmacy, food nutrition and animal husbandry. Therefore we can always rapidly provide solutions to our valued customers.<br>
                  For assisting customers to achieve optimal production, the products that we supplies have been approved by international standard laboratories. In order to ensure high-quality and high-efficiency product, and indeed meets product specifications and biological available efficiency.
                </p>
              </div>
              <div class="col-12">
                <p>
                  Under the principle of innovative, reasonable and transparent operations, the business has been flourishing. It has been winning the support of overseas suppliers and the trust of domestic customers.<br>
                  Forefront Enterprise Co., Ltd. s mission is always to focus on longtime customer base providing customized and professional services that exceed customer expectations.
                </p>
              </div>
            </div>',
        ]);

        About::create([
          'sub_id'=>'2',
        ]);


    }
}
