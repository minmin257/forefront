<?php

use Illuminate\Database\Seeder;
use App\History;

class HistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        History::create([
            'sub_id'=>'2',
        	'year'=>'1974',
        	'content'=>'丞泰企業有限公司創立，資本額新台幣4500萬。',
        	'en_content'=>'丞泰企業有限公司創立，資本額新台幣4500萬。',
        ]);

        History::create([
            'sub_id'=>'2',
        	'year'=>'1989',
        	'content'=>'新增服務項目：XXX、XXXXX',
        	'en_content'=>'新增服務項目：XXX、XXXXX',
        ]);

        History::create([
            'sub_id'=>'2',
        	'year'=>'1995',
        	'content'=>'公司遷址',
        	'en_content'=>'公司遷址',
        ]);

        History::create([
            'sub_id'=>'2',
        	'year'=>'2000',
        	'content'=>' 獲得多項輸入許可：<br>
                  XXXXX / XXXXX / XXXXX / XXXXX / XXXXX / XXXXX / XXXXX',
        	'en_content'=>' 獲得多項輸入許可：<br>
                  XXXXX / XXXXX / XXXXX / XXXXX / XXXXX / XXXXX / XXXXX',
        ]);

        History::create([
            'sub_id'=>'2',
        	'year'=>'2007',
        	'content'=>'增資XXXX，員工數達到XX人。',
        	'en_content'=>'增資XXXX，員工數達到XX人。',
        ]);

        History::create([
            'sub_id'=>'2',
        	'year'=>'2020',
        	'content'=>'官網正式改版上線。',
        	'en_content'=>'官網正式改版上線。',
        ]);

    }
}
