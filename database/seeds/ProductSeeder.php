<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'sub_id'=>'3',
        	'en_name'=>'Acarbose',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

        Product::create([
            'sub_id'=>'3',
        	'en_name'=>'Amoxicillin Trihydrate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

        Product::create([
            'sub_id'=>'3',
        	'en_name'=>'Ampicillin Sodium & Sulbactam Sodium (2:1)',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

        Product::create([
            'sub_id'=>'3',
        	'en_name'=>'Apixaban',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

        Product::create([
            'sub_id'=>'3',
        	'en_name'=>'Aripiprazole',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

        Product::create([
            'sub_id'=>'3',
        	'en_name'=>'Atorvastatin Calcium Trihydrate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

         Product::create([
            'sub_id'=>'3',
        	'en_name'=>'Betamethasone Dipropionate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

          Product::create([
            'sub_id'=>'3',
        	'en_name'=>'Betamethasone Sodium Phosphate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

           Product::create([
            'sub_id'=>'3',
        	'en_name'=>'Betamethasone Valerate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'3',
        	'en_name'=>'Bisoprolol Fumarate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'3',
        	'en_name'=>'Budesonide',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'3',
        	'en_name'=>'Cefaclor',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'3',
        	'en_name'=>'Cefadroxil Monohydrate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'3',
        	'en_name'=>'Cefalexin Monohydrate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'3',
        	'en_name'=>'Cefuroxime Axetil',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'3',
        	'en_name'=>'Cefuroxime Axetil Amorphous',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'3',
        	'en_name'=>'Clobetasol Propionate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'3',
        	'en_name'=>'Clobetasone Butyrate',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

//P2
            Product::create([
                'sub_id'=>'4',
        	'name'=>'氨康',
        	'en_name'=>'AminoGut',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'氯化膽鹼-二氧化矽型',
        	'en_name'=>'CHOLINE CHLORIDE 50％SB',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'氯化膽鹼-植物型',
        	'en_name'=>'CHOLINE CHLORIDE 60％CB',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'精氨酸',
        	'en_name'=>'L-Arginine',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'離氨酸',
        	'en_name'=>'L-LYSINE HCL F.G.',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'羥丁氨酸',
        	'en_name'=>'L-THREONINE 98.5％FEED',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'色氨酸',
        	'en_name'=>'L-TRYPTOPHAN FEED GRADE',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'纈胺酸',
        	'en_name'=>'L-Valine',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'紐滿乳',
        	'en_name'=>'NUTRIMAC 35',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'寶美肥（牛吃的脂肪粉）',
        	'en_name'=>'PALMIFAT',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'腸黏膜蛋白',
        	'en_name'=>'Peptone 50 Dried porcine Protein',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'甜乳清 CP 3％',
        	'en_name'=>'PERMEATE WHEY  POWDER SPRAY',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'普利乳',
        	'en_name'=>'PRELAC',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'素樂旺 CP 7％',
        	'en_name'=>'SEROLAT XL',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'素樂寶 CP 4％',
        	'en_name'=>'SEROLAT XXL',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'素樂肥',
        	'en_name'=>'SEROLAT 50',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'4',
        	'name'=>'甜乳清 CP 11％',
        	'en_name'=>'SWEET WHEY  POWDER FOOD G.',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);
//P3
            Product::create([
                'sub_id'=>'5',
        	'name'=>'氨康',
        	'en_name'=>'AminoGut',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'氯化膽鹼-二氧化矽型',
        	'en_name'=>'CHOLINE CHLORIDE 50％SB',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'氯化膽鹼-植物型',
        	'en_name'=>'CHOLINE CHLORIDE 60％CB',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'蛋氨酸',
        	'en_name'=>'DL-METHIONINE F.G.',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'精氨酸',
        	'en_name'=>'L-Arginine',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'離氨酸',
        	'en_name'=>'L-LYSINE HCL F.G.',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'羥丁氨酸',
        	'en_name'=>'L-THREONINE 98.5％FEED',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'色氨酸',
        	'en_name'=>'L-TRYPTOPHAN FEED GRADE',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'纈胺酸',
        	'en_name'=>'L-Valine',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'紐滿乳',
        	'en_name'=>'NUTRIMAC 35',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'寶美肥（牛吃的脂肪粉）',
        	'en_name'=>'PALMIFAT',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'腸黏膜蛋白',
        	'en_name'=>'Peptone 50 Dried porcine Protein',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'甜乳清 CP 3％',
        	'en_name'=>'PERMEATE WHEY  POWDER SPRAY',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'普利乳',
        	'en_name'=>'PRELAC',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'素樂旺 CP 7％',
        	'en_name'=>'SEROLAT XL',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'素樂寶 CP 4％',
        	'en_name'=>'SEROLAT XXL',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'素樂肥',
        	'en_name'=>'SEROLAT 50',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

            Product::create([
                'sub_id'=>'5',
        	'name'=>'甜乳清 CP 11％',
        	'en_name'=>'SWEET WHEY  POWDER FOOD G.',
        	'src'=>'https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600',
        ]);

    }
}
