<?php

use Illuminate\Database\Seeder;
use App\Contact;
class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
            'sub_id'=>'6',
        	'title'=>'丞泰企業有限公司 Forefront Enterprise Co., Ltd.', 
        	'logo'=>'/images/logo-lg.png',
        	'tel'=>'(02) 2713-3921',
        	'fax'=>'(02) 2713-0828',
        	'email'=>'forefront@afronts.com.tw',
        	'map'=>'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.437698572735!2d121.54149331519058!3d25.05314984367712!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abe744c9ad8d%3A0x411c664cd0c7aaa2!2z5Lie5rOw5LyB5qWt5pyJ6ZmQ5YWs5Y-4!5e0!3m2!1szh-TW!2stw!4v1595314214367!5m2!1szh-TW!2stw" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
        	'src'=>'/images/contact.jpg',
        	'content'=>'<p>歡迎來電聯繫！</p>',
        	'en_content'=>'<p>歡迎來電聯繫！</p>',
        ]);
    }
}
