<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserPermission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         User::create([
            'name'=>'admin',
            'account'=>'admin',
            'password'=>bcrypt('123456'),
            'state'=>1,
        ]);

         User::create([
            'name'=>'yuanpu',
            'account'=>'yuanpu',
            'password'=>bcrypt('123456'),
            'state'=>1,
        ]);

        $this->call(SubjectSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(SubSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(ContactSeeder::class);
        $this->call(HistorySeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(QuickLinkSeeder::class);
        $this->call(SystemSeeder::class);

        UserPermission::create([
            'user_id'=>'1',
            'permission_id'=>'1',
        ]);
        UserPermission::create([
            'user_id'=>'1',
            'permission_id'=>'2',
        ]);
        UserPermission::create([
            'user_id'=>'1',
            'permission_id'=>'3',

        ]);
       
    }
}
