<?php

use Illuminate\Database\Seeder;
use App\Banner;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banner::create([
        	'src'=>'/images/banner-01.jpg',
        	'en_src'=>'/images/banner-01.jpg',
        	'link'=>'#',
            'en_link'=>'#',
        ]);

        Banner::create([
        	'src'=>'/images/banner-02.jpg',
        	'en_src'=>'/images/banner-02.jpg',
        	'link'=>'#',
            'en_link'=>'#',
        ]);

        Banner::create([
        	'src'=>'/images/banner-03.jpg',
        	'en_src'=>'/images/banner-03.jpg',
        	'link'=>'#',
            'en_link'=>'#',
        ]);

        Banner::create([
        	'src'=>'/images/banner-04.jpg',
        	'en_src'=>'/images/banner-04.jpg',
        	'link'=>'#',
            'en_link'=>'#',
        ]);

    }
}
