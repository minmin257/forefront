<?php

use Illuminate\Database\Seeder;
use App\Sub;

class SubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sub::create([
        	'subject_id'=>'1',
        	'banner'=>'/images/banner-about.jpg',
        	'name'=>'公司簡介',
        	'en_name'=>'Company Profile',

        ]);

        Sub::create([
        	'subject_id'=>'1',
        	'banner'=>'/images/banner-about.jpg',
        	'name'=>'公司沿革',
        	'en_name'=>'History',
        ]);

        Sub::create([
        	'subject_id'=>'2',
        	'banner'=>'/images/banner-product01.jpg',
        	'name'=>'藥品原料',
        	'en_name'=>'API',
        	'fullname'=>'Active Pharmaceutical Ingredients
'
        ]);

         Sub::create([
        	'subject_id'=>'2',
        	'banner'=>'/images/banner-product02.jpg',
        	'name'=>'飼料原料',
        	'en_name'=>'Feed',
            'fullname'=>'Feed'
        ]);

          Sub::create([
        	'subject_id'=>'2',
        	'banner'=>'/images/banner-product03.jpg',
        	'name'=>'飼料添加劑',
        	'en_name'=>'Feed Additives',
            'fullname'=>'Feed Additives'
        ]);

          Sub::create([
            'subject_id'=>'3',
            'banner'=>'/images/banner-contact.jpg',
            'name'=>'聯絡我們',
            'en_name'=>'Contact',
        ]);


    }
}
