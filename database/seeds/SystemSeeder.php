<?php

use Illuminate\Database\Seeder;
use App\System;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        System::create([
        'sitename'=>'丞泰企業有限公司 Forefront Enterprise Co., Ltd.',
        'meta_description'=>'丞泰企業有限公司',
        'meta_keyword'=>'丞泰企業有限公司',
        'message'=>'本系統於進行設備維護，暫時無法提供服務。造成不便，敬請見諒。',
    	]);
        

    }
}
