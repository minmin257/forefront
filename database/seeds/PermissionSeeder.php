<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Permission::create([
            'name'=>'產品管理',

        ]);
        Permission::create([
            'name'=>'權限管理',
  
        ]);
        Permission::create([
            'name'=>'系統設定',

        ]);
        
    }
}
