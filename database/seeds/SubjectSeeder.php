<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
        	'name'=>'關於我們',
            'en_name'=>'About',
        ]);

        Subject::create([
        	'name'=>'產品與服務',
            'en_name'=>'Products',
        ]);

        Subject::create([
        	'name'=>'聯絡我們',
            'en_name'=>'Contact',
        ]);
    }
}
