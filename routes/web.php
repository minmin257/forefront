<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//前台
//維護中介
Route::group(['middleware'=>['maintenance']], function() {
	Route::get('/','FrontendController@index')->name('index');//前台首頁
	Route::get('/{subject}/{sub}','FrontendController@show')->name('page');
	//搜尋
	Route::get('/search','FrontendController@search')->name('search');
	//語言
	Route::get('/language/cookie/set','CookieController@setCookie')->name('set_cookie');
	Route::get('/language/cookie/get','CookieController@getCookie')->name('get_cookie');
});

//後台
Route::group(['prefix'=>'webmin','namespace'=>'backend'], function(){
	// 登入登出
	Route::get('/','LoginController@index')->name('backend');
	Route::post('/','LoginController@login')->name('login');
	Route::get('/backend/logout','LoginController@logout')->name('logout');
		
		//中介
	Route::group(['middleware'=>'auth:web'], function(){
		Route::get('/backend/index','ModuleController@index')->name('backend.index');
			//內容管理
			Route::group(['prefix'=>'content','namespace'=>'content'], function() {

				//首頁管理-首頁輪播
				Route::get('/home_banner', 'HomeBannerController@index')->name('backend.home_banner');
				Route::post('/home_banner', 'HomeBannerController@create')->name('create_home_banner');
				Route::post('/update_home_banner', 'HomeBannerController@update')->name('update_home_banner');
				Route::post('/delete_home_banner', 'HomeBannerController@delete')->name('delete_home_banner');

				//首頁管理-連結管理
				Route::get('/quicklink', 'QuickLinkController@index')->name('backend.quicklink');
				Route::post('/update_quicklink', 'QuickLinkController@update')->name('update_quicklink');

				//內頁banner管理
				Route::get('/inner_banner', 'InnerBannerController@index')->name('backend.inner_banner');
				Route::post('/update_inner_banner', 'InnerBannerController@update')->name('update_inner_banner');
				Route::post('/delete_inner_banner', 'InnerBannerController@delete')->name('delete_inner_banner');

				//公司簡介管理-頁面管理
				Route::get('/about', 'AboutController@index')->name('backend.about');
				Route::post('/create_about', 'AboutController@create')->name('create_about');
				Route::get('/about/{id}', 'AboutController@edit')->name('edit_about');
				Route::post('/about', 'AboutController@update_about')->name('update_about');
				Route::post('/about/{id}', 'AboutController@update_about_detail')->name('update_about_detail');
				Route::post('/delete_about', 'AboutController@delete')->name('delete_about');

				//公司簡介管理-公司沿革管理
				Route::get('/history', 'AboutController@history')->name('backend.history');
				Route::post('/history', 'AboutController@update_history')->name('update_history');//state
				Route::post('/create_history', 'AboutController@create_history')->name('create_history');
				Route::post('/delete_history', 'AboutController@delete_history')->name('delete_history');
				Route::get('/history/{id}', 'AboutController@edit_history')->name('edit_history');
				Route::post('/history/{id}', 'AboutController@update_history_detail')->name('update_history_detail');

			Route::group(['middleware'=>'HasPermission:產品管理'], function() {
				//產品資訊管理-分類管理
				Route::get('/productCategory', 'ProductCategoryController@index')->name('backend.category');
				Route::post('/productCategory', 'ProductCategoryController@update')->name('update_productCategory');
				Route::post('/create_productCategory', 'ProductCategoryController@create')->name('create_productCategory');
				Route::post('/delete_productCategory', 'ProductCategoryController@delete')->name('delete_productCategory');

				//產品資訊管理-產品管理
				Route::get('/product', 'ProductController@index')->name('backend.product');
				Route::post('/product', 'ProductController@update')->name('update_product');
				Route::post('/delete_product', 'ProductController@delete')->name('delete_product');
				Route::post('/delete_product_src', 'ProductController@delete_src')->name('delete_product_src');
				Route::get('/edit_product/{type}', 'ProductController@edit')->name('edit_product');		
				Route::post('/create_product', 'ProductController@create')->name('create_product');
			});	

				//聯絡我們管理
				Route::get('/contact', 'ContactController@index')->name('backend.contact');
				Route::post('/contact', 'ContactController@update')->name('update_contact');

			});

		// 權限管理
		Route::group(['prefix'=>'authority'], function() {
			//基本資料管理
			Route::get('/profile', 'AuthorityController@index')->name('backend.profile');
			Route::post('/profile', 'AuthorityController@update')->name('update_profile');
			
			Route::group(['middleware'=>'HasPermission:權限管理'], function() {
			//帳號管理
				Route::get('/management', 'AuthorityController@management')->name('backend.management');
				Route::get('/permission', 'AuthorityController@permission')->name('backend.permission');
				Route::get('/edit_user', 'AuthorityController@edit')->name('edit_user');
				Route::post('/delete_user', 'AuthorityController@delete')->name('delete_user');
				Route::post('/management', 'AuthorityController@create')->name('create_user');
				Route::post('/create_permission', 'AuthorityController@create_permission')->name('create_permission');
			});
		});

		Route::group(['prefix'=>'system'], function() {
			Route::get('/set', 'SystemController@index')->name('backend.system');
			Route::post('/set', 'SystemController@update')->name('update_system');
		});

	});		
});

