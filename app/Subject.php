<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'name','en_name'
    ];

    public function sub()
    {
    	return $this->hasMany(Sub::class)->where('delete',0)->orderBy('sort','desc');
    }
}
