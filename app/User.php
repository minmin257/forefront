<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'name', 'account', 'password', 'state', 'delete'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function permissions()
    {
        return $this->hasMany(UserPermission::class);
    }

    public function hasPermission(string $permission)
    {
        $permissions_id = $this->permissions()->pluck('permission_id');

        if(Permission::whereIn('id',$permissions_id)->where('name',$permission)->get()->count()>0)
        {
           return true; 
        }
        else
        {
           return false;
        }
    }
}
