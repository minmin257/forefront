<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $fillable = [
        'sub_id','year','content','en_content','state','delete'
    ];

    public function sub()
    {
    	return $this->belongsTo(Sub::class)->where('state',1);
    }
}
