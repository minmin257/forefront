<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SubjectRepository;

class HomeController extends Controller
{
    public function __construct(SubjectRepository $SubjectRepository){
    	
    	$this->subject = $SubjectRepository;
    }

    public function index(){
    	 return view('index');
    }
}
