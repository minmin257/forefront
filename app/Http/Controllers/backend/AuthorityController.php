<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\PermissionRepository;
use App\Http\Requests\backend\ProfileRequest;
use App\Http\Requests\backend\CreateUserRequest;

class AuthorityController extends Controller
{
	protected $UserRepository, $PermissionRepository;

    public function __construct(UserRepository $UserRepository, PermissionRepository $PermissionRepository)
    {
        $this->User = $UserRepository;
        $this->Permission = $PermissionRepository;
    }

    public function index()
    {
        $users = $this->User->read()->get();
        return view('backend.authority.index', compact('users'));
    }

    public function update(ProfileRequest $request)
    {
        $this->User->update($request);
        return back()->with('message', '更新完成');
    }

    public function management()
    {
        $users = $this->User->read()->where('id','!=',1)->get();
        return view('backend.authority.management',compact('users'));
    }

    public function permission(Request $request)
    {
        // dd($request->all());
        $user = $this->User->read()->findOrFail($request['id']);
        $permissions = $this->Permission->read();
        return view('backend.authority.permission',compact('user','permissions'));
    }

    public function create_permission(Request $request)
    {
        $this->User->delete_permission($request);
        $this->User->create_permission($request);
        return back()->with('message', '更新完成');
    }

    public function edit(Request $request)
    {
        $user = $this->User->read()->findOrFail($request['id']);
        return view('backend.authority.edit', compact('user'));
    }

    public function create(CreateUserRequest $request)
    {
        $this->User->create($request);
    }

    public function delete(Request $request)
    {
        $this->User->delete($request);
    }

    
}
