<?php

namespace App\Http\Controllers\backend\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\AboutRepository;
use App\Repositories\HistoryRepository;
use App\Repositories\SubRepository;
use App\Http\Requests\backend\UpdateAboutRequest;
use App\Http\Requests\backend\UpdateHistoryRequest;
use App\Http\Requests\backend\CreateHistoryRequest;
use App\Http\Requests\backend\CreateAboutRequest;
use DB;

class AboutController extends Controller
{
    protected $BannerRepository, $HistoryRepository, $SubRepository;

    public function __construct(AboutRepository $AboutRepository, HistoryRepository $HistoryRepository, SubRepository $SubRepository)
    {
        $this->About = $AboutRepository;
        $this->History = $HistoryRepository;
        $this->Sub = $SubRepository;
    }

    public function index(){

        $Abouts = DB::table('subs')
            ->orderBy('sort', 'desc')
            ->join('abouts', 'sub_id', '=', 'subs.id')
            ->select('abouts.*','subs.name', 'subs.en_name', 'subs.sort', 'subs.fullname', 'subs.state')
            ->where('subs.delete',0)
            ->get();
            // dd($Abouts);
    	return view('backend.content.about.index',compact('Abouts'));
    }

    public function edit($id)
    {
        
        $about = $this->About->read()->where('sub_id',$id)->first();

        return view('backend.content.about.edit', compact('about'));
    }

    public function create(CreateAboutRequest $request)
    {
        $this->Sub->about_name_create($request);
        $id = $this->Sub->read()->max('id');
        $this->About->create($id, $request);
    }

    public function update_about(Request $request)
    { 
        $this->Sub->state_update($request);
        return back()->with('message', '更新完成');
    }

    public function delete(Request $request)
    {
        $this->Sub->delete($request);
    }
    
    public function update_about_detail($id, UpdateAboutRequest $request)
    {
        $request->merge([
            'id'=>$id
        ]);
        $this->About->update($request);
        $this->Sub->name_update($request);
        return back()->with('message', '更新完成');
    }

    public function history(){
    	$Histories = $this->History->read()->get()->sortByDesc('year');
    	return view('backend.content.about.history',compact('Histories'));
    }

    public function delete_history(Request $request)
    {
        $this->History->delete($request);
    }

    public function update_history(Request $request)
    {
        
        $this->History->update($request);
        return back()->with('message', '更新完成');
    }

    public function create_history(CreateHistoryRequest $request)
    {
        $this->History->create($request);
    }
    
    public function edit_history($id)
    {
        $History = $this->History->read()->find($id);
        return view('backend.content.about.historyDetail', compact('History'));
    }

    public function update_history_detail(UpdateHistoryRequest $request)
    {
        $this->History->update_detail($request);
        return back()->with('message', '更新完成');
    }



}
