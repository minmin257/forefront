<?php

namespace App\Http\Controllers\backend\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\QuickLinkRepository;
use App\Http\Requests\backend\UpdateQuickLinkRequest;

class QuickLinkController extends Controller
{
    protected $QuickLinkRepository;

	public function __construct(QuickLinkRepository $QuickLinkRepository)
    {
        $this->Quicklink = $QuickLinkRepository;
    }

    public function index()
    {
        $Quicklinks = $this->Quicklink->read()->get();
        return view('backend.content.quicklink.index', compact('Quicklinks'));
    }

    public function update(UpdateQuickLinkRequest $request)
    {
        $this->Quicklink->update($request);
        return back()->with('message', '更新完成');
    }
}
