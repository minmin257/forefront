<?php

namespace App\Http\Controllers\backend\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SubRepository;

class InnerBannerController extends Controller
{
    protected $SubRepository;

	public function __construct(SubRepository $SubRepository)
    {
        $this->Sub = $SubRepository;
    }

    public function index()
    {
        $subs = $this->Sub->read()->get();
        return view('backend.content.innerbanner.index', compact('subs'));
    }

    public function update(Request $request)
    {
        $this->Sub->banner_update($request);
    }

    public function delete(Request $request)
    {
        $this->Sub->banner_delete($request);
    }
}
