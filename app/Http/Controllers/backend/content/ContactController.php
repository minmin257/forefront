<?php

namespace App\Http\Controllers\backend\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ContactRepository;
use App\Http\Requests\backend\UpdateContactRequest;

class ContactController extends Controller
{
    protected $ContactRepository;

	public function __construct(ContactRepository $ContactRepository)
    {
        $this->Contact = $ContactRepository;
    }

    public function index()
    {
        $contact = $this->Contact->read();
        return view('backend.content.contact.index', compact('contact'));
    }

    public function update(UpdateContactRequest $request)
    { 
        $this->Contact->update($request);
        return back()->with('message', '更新完成');
    }
}
