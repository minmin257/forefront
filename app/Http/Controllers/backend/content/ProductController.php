<?php

namespace App\Http\Controllers\backend\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use App\Repositories\SubRepository;
use App\Http\Requests\backend\UpdateProductRequest;
use App\Http\Requests\backend\CreateProductRequest;

class ProductController extends Controller
{
    protected $ProductRepository, $SubRepository;

	public function __construct(ProductRepository $ProductRepository, SubRepository $SubRepository)
    {
        $this->Product = $ProductRepository;
        $this->Sub = $SubRepository;

    }

    public function index(Request $request)
    {
        // dd($request->all());
        $productCategories = $this->Sub->read()->where('subject_id',2)->where('delete',0)->get();
        $products = $this->Product->chose_product($request);
        return view('backend.content.product.index', compact('products','productCategories'));
    }

    public function update(UpdateProductRequest $request)
    { 
        $this->Product->update($request);
        return back()->with('message', '更新完成');
    }

    public function delete(Request $request)
    {
        $this->Product->delete($request);
    }

    public function delete_src(Request $request)
    {
        $this->Product->delete_src($request);
    }

    public function create(CreateProductRequest $request)
    {
        $this->Product->create($request);
    }

    public function edit($id)
    {
        // dd($id);
        $productCategories = $this->Sub->read()->where('subject_id',2)->where('delete',0)->get();
        $Products = $this->Product->read()->where('sub_id',$id);
        $category = $this->Sub->read()->where('id',$id)->first();
        return view('backend.content.product.edit', compact('Products','productCategories','category'));
    }

}
