<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\BannerRepository;
use App\Http\Requests\backend\CreateBannerRequest;
use App\Http\Requests\backend\UpdateBannerRequest;

class HomeBannerController extends Controller
{
    protected $BannerRepository;

	public function __construct(BannerRepository $BannerRepository)
    {
        $this->Banner = $BannerRepository;
    }

    public function index()
    {
        $Banners= $this->Banner->read()->where('src','!=',null)->get()->sortByDesc('sort');
        $en_Banners= $this->Banner->read()->where('en_src','!=',null)->get()->sortByDesc('sort');
        return view('backend.content.homebanner.index', compact('Banners','en_Banners'));
    }

    public function create(CreateBannerRequest $request)
    {
        
        $this->Banner->create($request);
    }

    public function delete(Request $request)
    {
        $this->Banner->delete($request);
    }

     public function update(UpdateBannerRequest $request)
    {
        $this->Banner->update($request);
        return back()->with('message', '更新完成');
    }


}
