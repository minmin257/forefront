<?php

namespace App\Http\Controllers\backend\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SubRepository;
use App\Http\Requests\backend\CreateProductCategoryRequest;
use App\Http\Requests\backend\UpdateProductCategoryRequest;


class ProductCategoryController extends Controller
{
    protected $SubRepository;

	public function __construct(SubRepository $SubRepository)
    {
        $this->Sub = $SubRepository;
    }

    public function index()
    {
        $productCategories = $this->Sub->read()->where('subject_id',2)->where('delete',0)->get();
        return view('backend.content.productCategory.index', compact('productCategories'));
    }

    public function update(UpdateProductCategoryRequest $request)
    { 
        $this->Sub->product_category_update($request);
        return back()->with('message', '更新完成');
    }

    public function delete(Request $request)
    {
        $this->Sub->product_category_delete($request);
    }

    public function create(CreateProductCategoryRequest $request)
    {
        $this->Sub->product_category_create($request);
    }

}
