<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SystemRepository;
use App\Http\Requests\backend\SystemRequest;

class SystemController extends Controller
{
    protected $SystemRepository;

	public function __construct(SystemRepository $SystemRepository)
    {
        $this->System = $SystemRepository;
    }

    public function index()
    {
        $system = $this->System->read();
        return view('backend.system.index', compact('system'));
    }

    public function update(SystemRequest $request)
    {
    	$this->System->update($request);
    	return back()->with('message', '更新完成');
    }
}
