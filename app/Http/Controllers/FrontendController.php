<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Product;
use App\Repositories\SubjectRepository;
use App\Repositories\SubRepository;
use App\Repositories\AboutRepository;
use App\Repositories\ProductRepository;
use App\Repositories\HistoryRepository;
use App\Repositories\BannerRepository;
use App\Repositories\ContactRepository;
use App\Repositories\QuickLinkRepository;
use App\Http\Requests\SearchRequest;

class FrontendController extends Controller
{
    public function __construct(SubjectRepository $SubjectRepository, SubRepository $SubRepository, AboutRepository $AboutRepository, ProductRepository $ProductRepository, HistoryRepository $HistoryRepository, BannerRepository $BannerRepository, ContactRepository $ContactRepository, QuickLinkRepository $QuickLinkRepository){

    	$this->subject = $SubjectRepository;
    	$this->sub = $SubRepository;
    	$this->about = $AboutRepository;
    	$this->product = $ProductRepository;
    	$this->history = $HistoryRepository;
    	$this->banner = $BannerRepository;
    	$this->contact = $ContactRepository;
        $this->quick_link = $QuickLinkRepository;
    }

   
    public function index(){
   
    	$banners= $this->banner->read()->where('state','1')->where('src','!=',null)->get()->sortByDesc('sort');
        $en_banners= $this->banner->read()->where('en_state','1')->where('en_src','!=',null)->get()->sortByDesc('en_sort');
        $quick_links = $this->quick_link->read()->get();
        return view('frontend.index',compact('quick_links','banners','en_banners'));
    }

    public function show($subject,$id)
    {
       // dd('123');
        //依照subject選擇頁面跳轉
        $subject = $this->subject->read()->where('en_name',$subject)->first();
        //為分辨subject下是否有這個頁面
        // dd($sub);
        $sub = $this->sub->read()->where('id',$id)->first() ?? abort(404) ;
        $about = $this->about->read()->where('sub_id',$sub->id)->first();
        // dd($about);

        switch ($subject->id) {
				
            case '1':

            	if ($sub->name == '公司沿革') {

            		$histories = $this->history->read()->get()->sortByDesc('year')->where('state',1);
            		return view('frontend.about',compact('histories','subject','about','sub'));
            	}

               return view('frontend.about',compact('subject','about','sub'));
                break;   
            
            case '2':
	
	           	if ($sub->id != 6) {
                   $products = $this->product->read()->where('sub_id',$sub->id)->where('state',1);
                    return view('frontend.product',compact('products','sub','subject'));
                }abort(404);

		    default:

	    		$contact = $this->contact->read();
	    		return view('frontend.contact',compact('contact','sub','subject','about'));


        }
    }

    public function search(SearchRequest $request)
    {
        // dd($request->sub);
        $products = $this->product->search($request);

        $sub = $this->sub->read()->where('id',$request->sub)->first();
        $subject = $this->subject->read()->where('id',$sub->subject_id)->first();
        
        return view('frontend.search',compact('products','sub','subject'));
    }

}