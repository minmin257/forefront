<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'title' => ['required'],
            'filepath' => ['required'],
            'src_filepath' => ['required'],
            'tel' => ['required'],
            'fax' => ['required'],
            'map' => ['required'],
            'email' => ['required'],
            'content' => ['required'],
            'en_content' => ['required'],
        ];
    }

    public function attributes()
    {
        return [
            'en_content'=>'english content',
        ];
    }
}
