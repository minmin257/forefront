<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year'=>['required'],
            'content'=>['required'],
            'en_content'=>['required']
        ];
    }

    public function attributes()
    {
        return [
        'content'=>'content',
        'en_content'=>'english content',
        'year'=>'year'
        ];
    }
}
