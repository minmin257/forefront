<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id.*' => ['required','exists:banners,id'],
            'state.*' => ['required'],
            'filepath.*' => ['sometimes','required'],
            'en_filepath.*' => ['sometimes','required'],
            'link.*' => ['sometimes','nullable'],
            'en_link.*' => ['sometimes','nullable'],
            'sort.*' => ['required','numeric'],
        ];
    }

    public function attributes()
    {
        return [
            'en_link.*'=>'link',
            'link.*'=>'連結',
            'filepath.*'=>'圖片',
            'en_filepath.*'=>'filepath',
            'state.*'=>'state',
            'sort.*'=>'sort',
        ];
    }
}
