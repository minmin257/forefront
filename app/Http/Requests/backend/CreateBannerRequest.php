<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filepath' => ['sometimes','nullable'],
            'en_filepath' => ['sometimes','nullable'],
            'sort' => ['sometimes','required','numeric'],
            'link' => ['sometimes','nullable'],
            'en_link' => ['sometimes','nullable'],
            'en_sort' => ['sometimes','nullable'],
        ];
    }

    public function attributes()
    {
        return [
            'sort'=>'sort',
            'filepath'=>'filepath',
            'en_filepath'=>'english filepath',
            'en_sort' =>'english sort',
        ];
    }
}
