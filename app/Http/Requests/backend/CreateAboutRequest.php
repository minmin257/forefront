<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateAboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['required'],
            'en_name'=>['required'],
            'content'=>['nullable'], //應該要可空
            'en_content'=>['nullable'], //應該要可空
            'sort'=>['required','numeric'],

        ];
    }

    public function attributes()
    {
        return [
            'sort'=>'sort',
            'name'=>'name',
            'en_name'=>'english name',
            'en_content'=> 'english content'
        ];
    }
}
