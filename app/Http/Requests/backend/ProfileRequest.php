<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required','exists:users,id'],
            'name' => ['required','regex:/[\x{4e00}-\x{9fa5}a-zA-Z]$/u'],
            'password' => ['nullable','between:6,10','confirmed','regex:/(^[a-zA-Z0-9!$#%@\+-_]+$)/u'],
            'password_confirmation' => ['nullable'],
            'state' => ['sometimes','required','numeric'],
        ];
    }

}
