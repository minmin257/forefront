<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sort'=>['required'],
            'title' => ['sometimes', 'required'],
            'en_title' => ['sometimes', 'required'],
            'content' => ['sometimes'],
            'en_content' => ['sometimes'],
        ];
    }

    public function attributes()
    {
        return [
            'en_title'=>'title',
            'en_content'=>'content'
        ];
    }
}
