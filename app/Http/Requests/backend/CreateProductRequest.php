<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category'=>['required'],
            'name'=>['nullable'],
            'en_name'=>['required'],
            'sort'=>['required','numeric'],
            'filepath'=>['nullable'],

        ];
    }

    public function attributes()
    {
        return [
            'sort'=>'sort',
            'filepath'=>'filepath',
            'name'=>'name',
            'en_name'=>'english name',
            'category'=>'categorie',
        ];
    }
}
