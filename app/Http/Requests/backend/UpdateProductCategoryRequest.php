<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id.*'=>['required','exists:subs,id'],
            'name.*'=>['required'],
            'en_name.*'=>['required'],
            'fullname.*'=>['required'],
            'sort.*'=>['required','numeric'],
            'state.*'=>['required'],

        ];
    }

    public function attributes()
    {
        return [
            'sort.*'=>'sort',
            'name.*'=>'name',
            'en_name.*'=>'english name',
            'fullname.*'=>'english name(page)',
            'state.*'=>'state',
        ];
    }
}
