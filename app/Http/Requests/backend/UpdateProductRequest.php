<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id.*'=>['required','exists:products,id'],
            'categorie.*'=>['sometimes','required'],
            'name.*'=>['sometimes','nullable'],
            'en_name.*'=>['sometimes','required'],
            'sort.*'=>['required','numeric'],
            'state.*'=>['required'],
            'filepath.*'=>['sometimes','nullable'],
        ];
    }

    public function attributes()
    {
        return [
            'sort.*'=>'sort',
            'state.*'=>'state',
            'filepath.*'=>'filepath',
            'name.*'=>'name',
            'en_name.*'=>'english name',
            'categorie.*'=>'categorie',
        ];
    }
}
