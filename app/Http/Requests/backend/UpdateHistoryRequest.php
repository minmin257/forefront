<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required','exists:histories,id'],
            'state.*' => ['sometimes','required'],
            'year'=>['sometimes','required'],
            'content'=>['sometimes','required'],
            'en_content'=>['sometimes','required'],

        ];
    }

    public function attributes()
    {
        if (app()->getLocale() === 'zh-TW') {
            return [
            'en_content'=>'content',
            ];
        }
        else{
            return [
            'state.*'=>'state',
            'en_content'=>'content',
            'year'=>'year'
            ];
        }


    }
}
