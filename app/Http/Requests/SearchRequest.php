<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search'=>['required'],
            'sub'=>['required']
        ];
    }

    public function messages()
    {
        if(app()->getLocale() === 'zh_TW'){
            return [
                'sub.required'=>'請選擇分類',
                'search.required'=>'請輸入關鍵字',
            ];
        }
        else{
            return [
                'sub.required'=>'Please select a product category.',
                'search.required'=>'Please enter keywords.',
            ];
        }
    }
}
