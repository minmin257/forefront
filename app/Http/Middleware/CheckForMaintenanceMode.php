<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Closure;
use App\Repositories\SystemRepository;

class CheckForMaintenanceMode extends Middleware
{
    protected $System;

    public function __construct(SystemRepository $SystemRepository)
    {
        $this->System = $SystemRepository;
    }

    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    public function handle($request, Closure $next)
    {
        $system = $this->System->read();
    	if($system->maintain)
    	{		
    	    abort(503,$system->message);
    	}

    	return $next($request);
    }
}

