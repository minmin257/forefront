<?php

namespace App\Http\Middleware;

use Closure;
use Response;
use Illuminate\Support\Facades\Cookie;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = Cookie::get('lang');
        // dd($locale);
        if(is_null($locale))
        {
            Cookie::make('lang', 'zh_TW');
            // dd('123');
        }
        // dd(Cookie::get('lang'));
        app()->setLocale(Cookie::get('lang'));
        // dd(app()->getLocale());

        return $next($request);
    }
}
