<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'src','en_src','link','en_link','sort','en_sort','state','en_state','delete'
        //圖片 連結 順序 狀態 刪除
    ];

}
