<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\Product;


class ProductRepository
{
	protected $product;

	public function __construct(Product $Product){
    	$this->model = $Product;
    }

	public function read()
    {
        return $this->model->where('delete',0)->orderBy('sort','desc')->orderBy('sub_id','asc')->orderBy('en_name','asc')->get();
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $products = $this->model->where('sub_id', $request->sub)->where(function ($product) use ($request) {
		      $product->where('en_name','like', '%'.$request->search.'%')
          			  ->orWhere('name','like', '%'.$request->search.'%');	
		})->get();
        	// dd($products);
        return $products;
    }

    public function update(Request $request)
    {
        foreach($request['id'] as $key => $id)
            {
                $this->model->findOrFail($id)->update([
                    'sub_id'=>$request['category'][$key],
                    'name'=>$request['name'][$key],
                    'en_name'=>$request['en_name'][$key],
                    'src'=>$request['filepath'][$key],
                    'sort'=>$request['sort'][$key],
                    'state'=>$request['state'][$key],
                ]);
            }    
    }

    public function delete(Request $request)
    {
        $this->model->findOrFail($request['id'])->update([
            'delete'=>1,
        ]);
    }

    public function delete_src(Request $request)
    {
        $this->model->findOrFail($request['id'])->update([
            'src'=>null,
        ]);
    }

    public function create(Request $request)
    {
        $this->model->create([
            'sub_id'=>$request['category'],
            'name'=>$request['name'],
            'en_name'=>$request['en_name'],
            'src'=>$request['filepath'],
            'sort'=>$request['sort'],
        ]);
    }

    public function chose_product(Request $request){ //下拉分類
        // dd($request->id);
        if(is_null($request->category)) {
            $products = $this->read();
        }
        else{
            $products = $this->read()->where('sub_id',$request->category);
        }        
        return $products;
    }
}

