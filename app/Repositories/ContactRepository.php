<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\Contact;

class ContactRepository
{
	protected $contact;

	public function __construct(Contact $Contact){
    	$this->model = $Contact;
    }

	public function read()
    {
        return $this->model->first();
    }

    public function update(Request $request)
	{
		// dd($request->all());
		$this->model->findOrFail($request['id'])->update([
    		'title'=>$request['title'],
			'logo'=>$request['filepath'],
            'src'=>$request['src_filepath'],
			'tel'=>$request['tel'],
			'fax'=>$request['fax'],
			'map'=>$request['map'],
			'email'=>$request['email'],
			'content'=>$request['content'],
			'en_content'=>$request['en_content'],
    	]);
	}

}

?>