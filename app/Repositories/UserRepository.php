<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\User;
use App\UserPermission;

class UserRepository
{
	protected $user;

	public function __construct(User $User, UserPermission $UserPermission){
    	$this->model = $User;
        $this->UserPermission = $UserPermission;
    }

	public function read()
    {
        return $this->model->where('delete',0);
    }

    public function update(Request $request)
    {
        if($request->filled('password'))
        {
            if($request->has('state'))
            {
                $this->model->findOrFail($request['id'])->update([
                    'name'=>$request['name'],
                    'password'=>bcrypt($request['password']),
                    'state'=>$request['state'],
                ]);
            }
            else
            {
                $this->model->findOrFail($request['id'])->update([
                    'name'=>$request['name'],
                    'password'=>bcrypt($request['password']),
                ]);
            }
        }
        else
        {
            if($request->has('state'))
            {
                $this->model->findOrFail($request['id'])->update([
                    'name'=>$request['name'],
                    'state'=>$request['state'],
                ]);
            }
            else
            {
                $this->model->findOrFail($request['id'])->update([
                    'name'=>$request['name'],
                ]);
            }
        }
    }

    public function delete_permission(Request $request)
    {
        $this->UserPermission->where('user_id',$request['id'])->delete();
    }

    public function create_permission(Request $request)
    {
        foreach($request['state'] as $key => $value)//啟用的value
        {
            if($value)
            {
                $this->UserPermission->create([
                    'user_id'=>$request['id'],
                    'permission_id'=>$value,
                ]);
            }
        }
 
    }

    public function create(Request $request)
    {
        $user = $this->model->create([
            'account'=>$request['account'],
            'password'=>bcrypt($request['password']),
            'name'=>$request['name'],
            'state'=>$request['state'],
        ]);
    }

    public function delete(Request $request)
    {
        $this->model->findOrFail($request['id'])->update([
            'delete'=>1,
        ]);
    }
}

?>