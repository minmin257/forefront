<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\Subject;

class SubjectRepository
{
	protected $subject;

	public function __construct(Subject $Subject){
    	$this->model = $Subject;
    }

	public function read()
    {
        return $this->model->get();
    }
}

?>