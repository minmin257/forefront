<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\Banner;
use App\Http\Requests\backend\CreateBannerRequest;

class BannerRepository
{
	protected $banner;

	public function __construct(Banner $Banner){
    	$this->model = $Banner;
    }

	public function read()
    {
        return $this->model->where('delete',0)->orderBy('sort','desc');
    }

    public function create(CreateBannerRequest $request)
	{
		if ($request->filled('en_sort')){
			$this->model->create([
				'en_src'=>$request['en_filepath'],
				'en_link'=>$request['en_link'],
				'en_sort'=>$request['en_sort'],
			]);
		}
		else
		{
			$this->model->create([
				'src'=>$request['filepath'],
				'link'=>$request['link'],
				'sort'=>$request['sort'],
			]);
		}
			
	}

	public function delete(Request $request)
    {
    	if (app()->getLocale() === 'en') {
    		$this->model->findOrFail($request['id'])->update([
            	'en_src'=>null,
        	]);
    	}
    	else{
    		$this->model->findOrFail($request['id'])->update([
            	'src'=>null,
        	]);
    	}
    	
    }

    public function update(Request $request)
	{
		
		if($request->filled('id'))
		{
			foreach($request['id'] as $key => $id)
		    {
		    	if ($request->filled('en_state')) {
		    		$this->model->findOrFail($id)->update([
		    		'en_src'=>$request['en_filepath'][$key],
					'en_link'=>$request['en_link'][$key],
					'en_sort'=>$request['en_sort'][$key],
					'en_state'=>$request['en_state'][$key],
		        	]);
		    	}
		    	else{
		    		$this->model->findOrFail($id)->update([
		            'src'=>$request['filepath'][$key],
					'link'=>$request['link'][$key],
					'sort'=>$request['sort'][$key],
					'state'=>$request['state'][$key],
		        ]);
		    	}

		    	
		    }
		}
	}
}

?>