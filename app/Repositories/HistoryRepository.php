<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\History;

class HistoryRepository
{
	protected $history;

	public function __construct(History $History){
    	$this->model = $History;
    }

	public function read()
    {
        return $this->model->where('delete',0);
    }

    public function create(Request $request)
    {
        $this->model->create([
            'sub_id'=>2,
            'year'=>$request['year'],
            'content'=>$request['content'],
            'en_content'=>$request['en_content']
        ]);
    }

    public function delete(Request $request)
    {
        $this->model->findOrFail($request['id'])->update([
            'delete'=>1,
        ]);  
    }

    public function update(Request $request)
	{
		foreach($request['state'] as $key => $state)
	    {

	    	$this->model->findOrFail($key)->update([
	    		'state'=>$request['state'][$key],
	    	]);
	    
	    }
	}

	public function update_detail(Request $request)
	{
	    	
		if($request->filled('en_content'))	
	    	$this->model->findOrFail($request->id)->update([
	    		'year'=>$request->year,
	    		'en_content'=>$request->en_content
	    	]);
	    else{
	    	$this->model->findOrFail($request->id)->update([
	    		'year'=>$request->year,
	    		'content'=>$request->content
	    	]);
	    }
	
	}

}

?>