<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\QuickLink;

class QuickLinkRepository
{
	protected $QuickLink;

	public function __construct(QuickLink $QuickLink){
    	$this->model = $QuickLink;
    }

	public function read()
    {
        return $this->model;
    }

    public function update(Request $request)
	{
		
		if($request->filled('id'))
		{
			// dd($request->all());
			foreach($request['id'] as $key => $id)
		    {
		    	if ($request->has('en_name')) {
		    		$this->model->findOrFail($id)->update([
		    		'en_src'=>$request['en_filepath'][$key],
					'en_link'=>$request['en_link'][$key],
					'en_name'=>$request['en_name'][$key],
		            'src'=>$request['filepath'][$key],
		        	]);
		    	}
		    	else{
		    		$this->model->findOrFail($id)->update([
		    		'en_src'=>$request['en_filepath'][$key],
		            'src'=>$request['filepath'][$key],
		            'name'=>$request['name'][$key],
					'link'=>$request['link'][$key],

		        	]);
		    	}

		    	
		    }
		}
	}

	

}

?>