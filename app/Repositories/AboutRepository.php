<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\About;

class AboutRepository
{
	protected $about;

	public function __construct(About $About){
    	$this->model = $About;
    }

	public function read()
    {
        return $this->model;
    }

    public function update(Request $request)
    {
        if ($request->filled('en_title')){
            $this->model->findOrFail($request['id'])->update([
                'en_content'=>$request['en_content'],
            ]);
        } 
        else {
            $this->model->findOrFail($request['id'])->update([
                'content'=>$request['content'],
            ]);
        }
    }

    public function create($id, Request $request)
    {
        $this->model->create([
            'sub_id'=> $id,
            'content'=>$request['content'],
            'en_content'=>$request['en_content']
        ]);
    }
}

?>