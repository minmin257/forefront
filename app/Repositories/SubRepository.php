<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\Sub;
use App\About;

class SubRepository
{
	protected $sub;

	public function __construct(Sub $Sub){
    	$this->model = $Sub;
    }

	public function read()
    {
        return $this->model->where('delete',0)->orderBy('sort','desc');
    }

    public function banner_update(Request $request)
	{
		$this->model->findOrFail($request['id'])->update([
			'banner'=>$request['filepath'],
		]);
	}

	public function banner_delete(Request $request)
    {
    	$this->model->findOrFail($request['id'])->update([
            'banner'=>null,
        ]);
    }

    public function name_update(Request $request)
    {
        if ($request->filled('en_title')) {
            $this->model->findOrFail($request['sub_id'],'id')->update([
                'sort'=>$request['sort'],
                'en_name'=>$request['en_title'],
            ]);
        } else {
            $this->model->findOrFail($request['sub_id'],'id')->update([
                'sort'=>$request['sort'],
                'name'=>$request['title'],
            ]);
        }
    }

    public function state_update(Request $request)
    {
        // dd($request->all());
        foreach($request['state'] as $key => $state)
        {
            $this->model->findOrFail($key)->update([
                'state'=>$request['state'][$key],
            ]);
        
        }
    }

    public function delete(Request $request)
    {
        $this->model->findOrFail($request['id'])->update([
            'delete'=>1,
        ]);  
    }

    public function product_category_update(Request $request)
    {
        // dd($request->all());
        foreach($request['id'] as $key => $id)
        {
            $this->model->findOrFail($id)->update([
                'name'=>$request['name'][$key],
                'en_name'=>$request['en_name'][$key],
                'fullname'=>$request['fullname'][$key],
                'sort'=>$request['sort'][$key],
                'state'=>$request['state'][$key],
            ]);
        }    
    }

    public function product_category_create(Request $request)
    {
            $this->model->create([
                'subject_id'=>2,
                'name'=>$request['name'],
                'en_name'=>$request['en_name'],
                'fullname'=>$request['fullname'],
                'sort'=>$request['sort'],
            ]); 
    }

    public function product_category_delete(Request $request)
    {
        $this->model->findOrFail($request['id'])->update([
            'delete'=>1,
        ]);  
    }

    public function about_name_create(Request $request)
    {
        $this->model->create([
            'subject_id'=>1,
            'name'=>$request['name'],
            'en_name'=>$request['en_name'],
            'sort'=>$request['sort'],
        ]); 
    }

}

?>