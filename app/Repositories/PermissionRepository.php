<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Permission;

class PermissionRepository
{
    protected $Permission;

    public function __construct(Permission $Permission)
    {
        $this->model = $Permission;
    }

    public function read()
    {
        return $this->model->get();
    }

}
?>