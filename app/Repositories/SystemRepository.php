<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\System;

class SystemRepository
{
	protected $system;

	public function __construct(System $System){
    	$this->model = $System;
    }

	public function read()
    {
        return $this->model->first();
    }

    public function update(Request $request)
	{
		$this->model->first()->update([
            'sitename'=>$request['sitename'],
            'meta_description'=>$request['meta_description'],
            'meta_keyword'=>$request['meta_keyword'],
            'maintain'=>$request['maintain'],
            'message'=>$request['message'],
        ]);
	}
}

?>