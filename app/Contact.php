<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'sub_id','title', 'logo','tel','fax','email','map','src','content','en_content'
    ];
    
    public function sub()
    {
    	return $this->belongsTo(Sub::class)->where('state',1);
    }
}
