<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub extends Model
{
    protected $fillable = [
        'subject_id','name','en_name','fullname','banner','sort','state','delete'

    ];

    public function subject()
    {
    	return $this->belongsTo(Subject::class)->where('state',1);
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function contact()
    {
        return $this->hasOne(Contact::class);
    }

    public function history()
    {
        return $this->hasOne(History::class);
    }

    public function about()
    {
        return $this->hasMany(About::class);
    }
}
