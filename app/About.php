<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
        'sub_id','content','en_content'
        //圖文編輯器 中英內容
    ];

    public function sub()
    {
        return $this->belongsTo(Sub::class)->where('state',1);
    }
}
