<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'sub_id','name','en_name','src','sort','state','delete'
    ];

    public function sub()
    {
    	return $this->belongsTo(Sub::class)->where('state',1);
    }
}
