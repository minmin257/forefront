<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    protected $fillable = [
        'sitename', 'meta_keyword','meta_description','maintain', 'message'
        //網站名稱 MetaMessage 維護 維護訊息
    ];
}
