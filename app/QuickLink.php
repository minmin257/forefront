<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickLink extends Model
{
    protected $fillable = [
        'name','en_name','link','en_link','src','en_src'
    ];
}
