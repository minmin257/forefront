@extends('layout.master')
@inject('subs', 'App\Repositories\SubRepository')
@section('header')

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        @if(app()->getLocale() === 'en')
          <h5 class="modal-title" id="exampleModalLabel">Product Search</h5>
        @else
          <h5 class="modal-title" id="exampleModalLabel">產品搜尋</h5>
        @endif
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="{{ route('search') }}" method="GET">
        {{-- <form id="header_search"> --}}
          <div class="form-group">
            <select class="form-control" name="sub" onchange="">
                @if(app()->getLocale() === 'en')
                  <option value="">Please select product category</option>
                @else
                  <option value="">請選擇產品分類</option>
                @endif

              @foreach ($subs->read()->where('subject_id','2')->where('state',1)->get() as $key => $sub)
                @if(app()->getLocale() === 'en')
                  <option value="{{ $sub->id }}">{{ $sub->en_name }}</option>
                @else
                  <option value="{{ $sub->id }}">{{ $sub->name }}</option>
                @endif
                
              @endforeach
            </select>
          </div>
          <div class="input-group">
            @if(app()->getLocale() === 'en')
              <input name="search" type="text" class="form-control" placeholder="Product Search
，Please enter key words" aria-describedby="button-addon2">
            @else
               <input name="search" type="text" class="form-control" placeholder="產品搜尋，請輸入關鍵字" aria-describedby="button-addon2">
            @endif
            <div class="input-group-append">
              <button class="btn btn-006300" type="submit" id="button-addon2">
                <i class="fa fa-search"></i>
              </button>
            </div>
          </div>
          @if ($errors->any())
              <div class="alert alert-danger" style="display: none;" id="errormessage">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </div>
          @endif
        </form>
      </div>
    </div>
  </div>
</div>

@endsection