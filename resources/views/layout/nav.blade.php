@extends('layout.master')
@inject('subject', 'App\Repositories\SubjectRepository')
@php
  $subs = App\Sub::all();
  $contact = App\Contact::first();
@endphp
@section('nav')

<div class="header">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand wow fadeInDown" href="{{ route('index') }}">
        <h1 class="sr-only">{{ $contact->title }}"></h1>
        <img src="/images/logo-text.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse align-self-end" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto wow fadeInUp">
{{-- subject --}}
        @if(isset($subject))
          
           @foreach ($subject->read() as $subject)
          
            @if ($subject->id == 3 )
              <li class="nav-item">
                @if(app()->getLocale() === 'en')
                  <a class="nav-link" href="{{ route('page',['subject'=> $subject->en_name , 'sub' => '6']) }}">Contact</a>
                @else
                  <a class="nav-link" href="{{ route('page',['subject'=> $subject->en_name , 'sub' => '6']) }}">聯絡我們</a>
                @endif
                
              </li>

            @else  

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  @if(app()->getLocale() === 'en')
                    {{ $subject->en_name }}
                  @else
                    {{ $subject->name }}
                  @endif
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              
                  @foreach ($subs->where('subject_id',$subject->id)->where('state',1)->where('delete',0)->sortByDesc('sort') as $sub)
                      <a class="dropdown-item" href="{{ route('page', ['subject'=> $subject->en_name , 'sub'=> $sub->id]) }}">
                        @if(app()->getLocale() === 'en')
                            {{ $sub->en_name }}
                        @else
                            {{ $sub->name }}
                        @endif
                      </a>
                  @endforeach

                </div>
              </li>

            @endif

           @endforeach
        @endif
{{-- subject --}}

          <li class="nav-item">
            <div class="nav-link">
              <a class="btn-006300r" data-toggle="modal" data-target="#exampleModal" href="javascript:;">
                @if(app()->getLocale() === 'en')
                    <i class="fa fa-search"></i> Search
                @else
                    <i class="fa fa-search"></i> 搜尋
                @endif
                
              </a>
            </div>
          </li>
            
            <li class="nav-item">
            <div class="nav-link">
              <div class="btn-006300r">
                
                <a class="language" id="zh_TW" href="{{ route('set_cookie', ['lang'=>'zh_TW']) }}">中文</a>│ 
                <a class="language" id="en" href="{{ route('set_cookie', ['lang'=>'en']) }}">English</a>

              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>

@endsection
