@extends('layout.master')
@php
  $contact = App\Contact::first();
@endphp
@section('footer')
@if(isset($contact))
<div class="footer">
      <div class="info">
        <div class="container">
          <div class="row justify-content-between align-items-center">
            <div class="col-12 col-xl">
              <h4>{{ $contact->title }}</h4>
            </div>
            <div class="col-12 col-xl info-item-block">
              <span class="info-item">
                <i class="fa fa-phone-alt"></i>
                <a href="tel:{{ preg_replace('/[^\d\#]/','',$contact->tel) }}" title="">
                  {{ $contact->tel }}
                </a>
              </span>│
              <span class="info-item">
                <i class="fa fa-fax"></i>
                {{ $contact->fax }}
              </span>│
              <span class="info-item">
                <i class="fa fa-envelope"></i>
                <a href="mailto:{{ $contact->email }}">
                  {{ $contact->email }}
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
        <div class="container">
          <div class="d-flex justify-content-between">
            <div class="tl">
              Copyright © Forefront Enterprise Co., Ltd. All rights reserved.
            </div>
            <div class="tr">
              <a href="http://www.yuan-pu.com.tw/" target="_blank">Designed by YUANPU</a>
            </div>
          </div>
        </div>        
      </div>
    </div>
@endif


    <a class="btn-goTop" href="javascript:;">
      <i class="fas fa-angle-double-up"></i>
    </a>
@endsection