@inject('system', 'App\Repositories\SystemRepository')
@php
  $contact = App\Contact::first();
@endphp
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- 網站標題icon -->
    <title>{{ $system->read()->sitename }}</title>
    <link rel="shortcut icon" href="/images/logo-icon.png" />
    <meta name="keywords" content="{{ $system->read()->meta_keyword }}">
    <meta name="description" content="{{ $system->read()->meta_description }}">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="/css/bootstrap_r.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/lightbox.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

  </head>
  <body>
    <div class="wrap">
        @yield('header')
        @yield('nav')
        @yield('banner')
        @yield('content')
    </div>

        @yield('footer')

    <!-- JavaScript -->
    <script src="/js/jquery.1.11.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/wow.js"></script>
    <script src="/js/lightbox.js"></script>
    <script src="https://kit.fontawesome.com/588be6838c.js"></script>
    <script src="/js/yp.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.css">
    <script src="sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script>
      $(function(){
        console.log()
        if($('#errormessage').html() !== undefined){
          Swal.fire({
            html: '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">'+$('#errormessage').html()+'</span>',
            icon: 'error',
            showConfirmButton: false,
            width: '300px',
            heightAuto: false,
            timer: 3000
          })
        }
      })
    </script>
    @yield('js')
  </body>
</html>