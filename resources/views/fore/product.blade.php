<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- 網站標題icon -->
    <title>丞泰企業有限公司 Forefront Enterprise Co., Ltd.</title>
    <link rel="shortcut icon" href="images/logo-icon.png" />
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="css/bootstrap_r.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/lightbox.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
    <div class="wrap">
      <!-- header==================================================== -->
      <div class="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container">
            <a class="navbar-brand wow fadeInDown" href="index.html">
              <h1 class="sr-only">丞泰企業有限公司 Forefront Enterprise Co., Ltd."></h1>
              <img src="images/logo-text.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse align-self-end" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto wow fadeInUp">

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    關於我們
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="about.html">公司簡介</a>
                    <a class="dropdown-item" href="history.html">公司沿革</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    產品與服務
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="product-01.html">藥品原料</a>
                    <a class="dropdown-item" href="product-02.html">飼料原料</a>
                    <a class="dropdown-item" href="product-03.html">飼料添加劑</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="contact.html">聯絡我們</a>
                </li>
                
                <li class="nav-item">
                  <div class="nav-link">
                    <a class="btn-006300r" data-toggle="modal" data-target="#exampleModal" href="javascript:;">
                      <i class="fa fa-search"></i> 搜尋
                    </a>
                  </div>
                </li>
                
                <li class="nav-item">
                  <div class="nav-link">
                    <div class="btn-006300r">
                      <a class="language" href="index.html">中文
                      </a> │ 
                      <a class="language" href="index-en.html">English
                      </a>
                    </div>
                  </div>
                </li>

              </ul>
            </div>
          </div>
        </nav>
      </div>
      <!-- banner==================================================== -->
      <div class="banner">
        <img src="images/banner-product01.jpg" alt="">        
      </div>
      <!-- content=================================================== -->
      <content>
        <div class="container">
           <!-- 頁面標題 -->
          <div class="title">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">首頁</a></li>
                <li class="breadcrumb-item">產品與服務</li>
                <li class="breadcrumb-item"><a href="contact.html">藥品原料</a></li>
              </ol>
            </nav>
            <h2>
              <strong>產品與服務</strong>
            </h2>
            <h3>
              藥品原料
            </h3>
          </div>
          <!-- 內容 -->
          <div class="content">
            <!-- 搜尋 -->
            <div class="row justify-content-center mb-4">
              <div class="col-12 col-md-8 col-lg-6">
                <form>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="產品搜尋，請輸入關鍵字" aria-describedby="button-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-006300" type="button" id="button-addon2">
                        <i class="fa fa-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- 產品列表 -->
            <table class="table table-borderless table-striped table-hover">
              <tbody>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Acarbose
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Acarbose">
                      <i class="fa fa-picture-o"></i>
                    </a>
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Amoxicillin Trihydrate
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Amoxicillin Trihydrate">
                      <i class="fa fa-picture-o"></i>
                    </a> -->                    
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Ampicillin Sodium & Sulbactam Sodium (2:1)
                  </td>
                  <td>
                     
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Ampicillin Sodium & Sulbactam Sodium (2:1)">
                      <i class="fa fa-picture-o"></i>
                    </a> -->
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Apixaban
                  </td>
                  <td>
                     
                  </td>
                  <td>
                    <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Apixaban">
                      <i class="fa fa-picture-o"></i>
                    </a>                    
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Aripiprazole
                  </td>
                  <td>
                     
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Aripiprazole">
                      <i class="fa fa-picture-o"></i>
                    </a> -->
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Atorvastatin Calcium Trihydrate
                  </td>
                  <td>
                     
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Atorvastatin Calcium Trihydrate">
                      <i class="fa fa-picture-o"></i>
                    </a> -->                    
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Betamethasone Dipropionate
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Betamethasone Dipropionate">
                      <i class="fa fa-picture-o"></i>
                    </a>
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Betamethasone Sodium Phosphate
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Betamethasone Sodium Phosphate">
                      <i class="fa fa-picture-o"></i>
                    </a> -->                    
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Betamethasone Valerate
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Betamethasone Valerate">
                      <i class="fa fa-picture-o"></i>
                    </a> -->
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Bisoprolol Fumarate
                  </td>
                  <td>
                     
                  </td>
                  <td>
                    <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Bisoprolol Fumarate">
                      <i class="fa fa-picture-o"></i>
                    </a>                    
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Budesonide
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Budesonide">
                      <i class="fa fa-picture-o"></i>
                    </a>
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Cefaclor
                  </td>
                  <td>
                     
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Cefaclor">
                      <i class="fa fa-picture-o"></i>
                    </a> -->                    
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Cefadroxil Monohydrate
                  </td>
                  <td>
                     
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Cefadroxil Monohydrate">
                      <i class="fa fa-picture-o"></i>
                    </a> -->
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Cefalexin Monohydrate
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Cefalexin Monohydrate">
                      <i class="fa fa-picture-o"></i>
                    </a> -->                    
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Cefuroxime Axetil
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Cefuroxime Axetil">
                      <i class="fa fa-picture-o"></i>
                    </a> -->
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Cefuroxime Axetil Amorphous
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Cefuroxime Axetil Amorphous">
                      <i class="fa fa-picture-o"></i>
                    </a>  -->                   
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Clobetasol Propionate
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Clobetasol Propionate">
                      <i class="fa fa-picture-o"></i>
                    </a> -->
                  </td>
                </tr>
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    Clobetasone Butyrate
                  </td>
                  <td>
                    
                  </td>
                  <td>
                    <!-- <a href="https://api.fnkr.net/testimg/800x600/cccccc/FFF/?text=800x600" data-lightbox="product" title="Clobetasone Butyrate">
                      <i class="fa fa-picture-o"></i>
                    </a> -->                    
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </content>
      
    </div>
    <!-- footer====================================================== -->

    <!-- JavaScript -->
    <script src="js/jquery.1.11.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/lightbox.js"></script>
    <script src="https://kit.fontawesome.com/588be6838c.js"></script>
    <script src="js/yp.js"></script>
  </body>
</html>