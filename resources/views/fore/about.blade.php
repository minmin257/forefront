<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- 網站標題icon -->
    <title>丞泰企業有限公司 Forefront Enterprise Co., Ltd.</title>
    <link rel="shortcut icon" href="images/logo-icon.png" />
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="css/bootstrap_r.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/lightbox.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
    <div class="wrap">
      <!-- header==================================================== -->
      <div class="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container">
            <a class="navbar-brand wow fadeInDown" href="index.html">
              <h1 class="sr-only">丞泰企業有限公司 Forefront Enterprise Co., Ltd."></h1>
              <img src="images/logo-text.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse align-self-end" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto wow fadeInUp">

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    關於我們
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="about.html">公司簡介</a>
                    <a class="dropdown-item" href="history.html">公司沿革</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    產品與服務
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="product-01.html">藥品原料</a>
                    <a class="dropdown-item" href="product-02.html">飼料原料</a>
                    <a class="dropdown-item" href="product-03.html">飼料添加劑</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="contact.html">聯絡我們</a>
                </li>
                
                <li class="nav-item">
                  <div class="nav-link">
                    <a class="btn-006300r" data-toggle="modal" data-target="#exampleModal" href="javascript:;">
                      <i class="fa fa-search"></i> 搜尋
                    </a>
                  </div>
                </li>
                
                <li class="nav-item">
                  <div class="nav-link">
                    <div class="btn-006300r">
                      <a class="language" href="index.html">中文
                      </a> │ 
                      <a class="language" href="index-en.html">English
                      </a>
                    </div>
                  </div>
                </li>

              </ul>
            </div>
          </div>
        </nav>
      </div>
      <!-- banner==================================================== -->
      <div class="banner">
        <img src="images/banner-about.jpg" alt="">        
      </div>
      <!-- content=================================================== -->
      <content>
        <div class="container">
           <!-- 頁面標題 -->
          <div class="title">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">首頁</a></li>
                <li class="breadcrumb-item">關於我們</li>
                <li class="breadcrumb-item"><a href="about.html">公司簡介</a></li>
              </ol>
            </nav>
            <h2>
              <strong>關於我們</strong>
            </h2>
            <h3>
              公司簡介
            </h3>
          </div>
          <!-- 內容 -->
          <div class="content">
            <!-- ↓圖文編輯器↓ -->
            <div class="row mb-3">
              <div class="col-12 col-lg-8">
                <p>
                  丞泰企業有限公司創立於1974年11月22日，資本額新台幣4500萬，以誠信為立業基礎，以穩健為經營方針，迄今已逾45年。<br>
                  丞泰企業有限公司主要營業項目為：藥品原料、飼料添加劑等相關產品之進口與行銷業務。藥品部分有--西藥原料、維生素抗生素、動物用藥品原料、藥品輔料、中間體原料等，飼料添加劑部分有--維生素、乳製品、氨基酸等。在創新、合理、透明的管理原則下業務蒸蒸日上，深獲國外供應商的支持與國內顧客的信賴，更獲得業界好評。
                </p>
              </div>
              <div class="col-12 col-sm-6 col-lg-4">
                <img src="images/about-01.jpg" class="w-100 mb-3" alt="">
              </div>
              <div class="col-12 col-sm-6 col-lg-4">
                <img src="images/about-02.jpg" class="w-100 mb-3" alt="">
              </div>
              <div class="col-12 col-lg-8">
                <p>
                  丞泰企業有限公司逾45年的誠信經營，已經奠定穩固的發展基礎，健全的財務結構及與顧客建立的長久夥伴關係，是促進業務成長的利基，所代理國外產品之供應商更是龐大的支持系統，造就了丞泰企業有限公司不斷的進步與發展。<br>
                  丞泰企業有限公司專精於藥品、飼料的進口與行銷，專業的行銷人員具有豐富的藥學、畜牧方面的專業知識與實務經驗，能迅速提供客戶各式問題的解決方案。為協助客戶的生產達到最佳化，丞泰企業有限公司所進口的產品均經過國際標準的實驗室檢驗，以確保每項產品皆是高品質與高效能之產品，並確實符合產品規格與生物可利用效率。
                </p>
              </div>
              <div class="col-12">
                <p>
                  丞泰企業有限公司的使命是：關注我們的顧客，提供超越顧客期望的產品與服務，建立與顧客長久的夥伴關係。<br>
                  丞泰企業有限公司的願景是：成為台灣藥品原料、飼料添加劑的首屈供應者。<br>
                  展望未來，丞泰企業有限公司將擴大經營規模，延聘優秀專業人才，開創新局，其成果將與所有員工共享。
                </p>
              </div>
            </div>
            <!-- ↑圖文編輯器↑ -->
          </div>
        </div>
      </content>
      
    </div>
    <!-- footer====================================================== -->
    <div class="footer">
      <div class="info">
        <div class="container">
          <div class="row justify-content-between align-items-center">
            <div class="col-12 col-xl">
              <h4>丞泰企業有限公司 Forefront Enterprise Co., Ltd.</h4>
            </div>
            <div class="col-12 col-xl info-item-block">
              <span class="info-item">
                <i class="fa fa-phone-alt"></i>
                <a href="tel:0227133921" title="">
                  (02) 2713-3921
                </a>
              </span>│
              <span class="info-item">
                <i class="fa fa-fax"></i>
                (02) 2713-0828
              </span>│
              <span class="info-item">
                <i class="fa fa-envelope"></i>
                <a href="mailto:forefront@afronts.com.tw">
                  forefront@afronts.com.tw
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
        <div class="container">
          <div class="d-flex justify-content-between">
            <div class="tl">
              Copyright © Forefront Enterprise Co., Ltd. All rights reserved.
            </div>
            <div class="tr">
              <a href="http://www.yuan-pu.com.tw/" target="_blank">Designed by YUANPU</a>
            </div>
          </div>
        </div>        
      </div>
    </div>



    <a class="btn-goTop" href="javascript:;">
      <i class="fas fa-angle-double-up"></i>
    </a>









    <!-- Modal===================================================== -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">產品搜尋</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <select class="form-control">
                  <option value="">請選擇產品分類</option>}
                  <option value="">藥品原料</option>
                  <option value="">飼料原料</option>
                  <option value="">飼料添加劑</option>
                </select>
              </div>
              <div class="input-group">
                <input type="text" class="form-control" placeholder="請輸入關鍵字" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-006300" type="button" id="button-addon2">
                    <i class="fa fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>






    <!-- JavaScript -->
    <script src="js/jquery.1.11.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/lightbox.js"></script>
    <script src="https://kit.fontawesome.com/588be6838c.js"></script>
    <script src="js/yp.js"></script>
  </body>
</html>