<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- 網站標題icon -->
    <title>丞泰企業有限公司 Forefront Enterprise Co., Ltd.</title>
    <link rel="shortcut icon" href="images/logo-icon.png" />
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="css/bootstrap_r.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/lightbox.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
    <div class="wrap">
      <!-- header==================================================== -->
      <div class="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container">
            <a class="navbar-brand wow fadeInDown" href="index.html">
              <h1 class="sr-only">丞泰企業有限公司 Forefront Enterprise Co., Ltd."></h1>
              <img src="images/logo-text.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse align-self-end" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto wow fadeInUp">

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    關於我們
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="about.html">公司簡介</a>
                    <a class="dropdown-item" href="history.html">公司沿革</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    產品與服務
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="product-01.html">藥品原料</a>
                    <a class="dropdown-item" href="product-02.html">飼料原料</a>
                    <a class="dropdown-item" href="product-03.html">飼料添加劑</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="contact.html">聯絡我們</a>
                </li>
                
                <li class="nav-item">
                  <div class="nav-link">
                    <a class="btn-006300r" data-toggle="modal" data-target="#exampleModal" href="javascript:;">
                      <i class="fa fa-search"></i> 搜尋
                    </a>
                  </div>
                </li>
                
                <li class="nav-item">
                  <div class="nav-link">
                    <div class="btn-006300r">
                      <a class="language" href="index.html">中文
                      </a> │ 
                      <a class="language" href="index-en.html">English
                      </a>
                    </div>
                  </div>
                </li>

              </ul>
            </div>
          </div>
        </nav>
      </div>
      <!-- banner==================================================== -->
      <div class="banner">
        <img src="images/banner-contact.jpg" alt="">        
      </div>
      <!-- content=================================================== -->
      <content>
        <div class="container">
           <!-- 頁面標題 -->
          <div class="title">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">首頁</a></li>
                <li class="breadcrumb-item"><a href="contact.html">聯絡我們</a></li>
              </ol>
            </nav>
            <h2>
              <strong>聯絡我們</strong>
            </h2>
            <!-- <h3>
              聯絡我們
            </h3> -->
          </div>
          <!-- 內容 -->
          <div class="content">
            <div class="row">
              <div class="col-12 col-md-9 order-md-2">
                <div class="company-title">
                  <h4 class="sr-only">丞泰企業有限公司 Forefront Enterprise Co., Ltd.</h4>
                  <img src="images/logo-lg.png" alt="">
                </div>
                <div class="company-info">
                  <div>
                    <i class="fa fa-phone-alt"></i>：
                    <a href="tel:0227133921" title="">
                      (02) 2713-3921
                    </a>
                  </div>
                  <div>
                    <i class="fa fa-fax"></i>：
                    (02) 2713-0828
                  </div>
                  <div>
                    <i class="fa fa-envelope"></i>：
                    <a href="mailto:forefront@afronts.com.tw">
                      forefront@afronts.com.tw
                    </a>
                  </div>
                  <div>
                    <!-- ↓圖文編輯器↓ -->
                    <p>
                      歡迎來電聯繫！
                    </p>
                    <!-- ↑圖文編輯器↑ -->
                  </div>
                </div>
              </div>
              <div class="col-12 order-md-2">
                <div class="map">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.437698572735!2d121.54149331519058!3d25.05314984367712!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abe744c9ad8d%3A0x411c664cd0c7aaa2!2z5Lie5rOw5LyB5qWt5pyJ6ZmQ5YWs5Y-4!5e0!3m2!1szh-TW!2stw!4v1595314214367!5m2!1szh-TW!2stw" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
              </div>
              <div class="col-12 col-md-3 order-md-1">
                <img src="images/contact.jpg" class="w-100" alt="">
              </div>
            </div>
          </div>
        </div>
      </content>
      
    </div>
    <!-- footer====================================================== -->
    <div class="footer">
      <div class="info">
        <div class="container">
          <div class="row justify-content-between align-items-center">
            <div class="col-12 col-xl">
              <h4>丞泰企業有限公司 Forefront Enterprise Co., Ltd.</h4>
            </div>
            <div class="col-12 col-xl info-item-block">
              <span class="info-item">
                <i class="fa fa-phone-alt"></i>
                <a href="tel:0227133921" title="">
                  (02) 2713-3921
                </a>
              </span>│
              <span class="info-item">
                <i class="fa fa-fax"></i>
                (02) 2713-0828
              </span>│
              <span class="info-item">
                <i class="fa fa-envelope"></i>
                <a href="mailto:forefront@afronts.com.tw">
                  forefront@afronts.com.tw
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
        <div class="container">
          <div class="d-flex justify-content-between">
            <div class="tl">
              Copyright © Forefront Enterprise Co., Ltd. All rights reserved.
            </div>
            <div class="tr">
              <a href="http://www.yuan-pu.com.tw/" target="_blank">Designed by YUANPU</a>
            </div>
          </div>
        </div>        
      </div>
    </div>



    <a class="btn-goTop" href="javascript:;">
      <i class="fas fa-angle-double-up"></i>
    </a>









    <!-- Modal===================================================== -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">產品搜尋</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <select class="form-control">
                  <option value="">請選擇產品分類</option>}
                  <option value="">藥品原料</option>
                  <option value="">飼料原料</option>
                  <option value="">飼料添加劑</option>
                </select>
              </div>
              <div class="input-group">
                <input type="text" class="form-control" placeholder="請輸入關鍵字" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-006300" type="button" id="button-addon2">
                    <i class="fa fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>






    <!-- JavaScript -->
    <script src="js/jquery.1.11.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/lightbox.js"></script>
    <script src="https://kit.fontawesome.com/588be6838c.js"></script>
    <script src="js/yp.js"></script>
  </body>
</html>