@extends('layout.default')
@section('banner')

  <div class="banner">
    <img src="{{ $sub->banner }}" alt="">        
  </div>

@endsection
@section('content')
<content>
    <div class="container">
           <!-- 頁面標題 -->
          <div class="title">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">
                	@if(app()->getLocale() === 'en')
                  		Home
                	@else
                  		首頁
                	@endif
                </a></li>
                <li class="breadcrumb-item">
                	@if(app()->getLocale() === 'en')
                  		{{ $subject->en_name }}
                	@else
                  		{{ $subject->name }}
                	@endif
                </li>
                
              </ol>
            </nav>
            <h2>
            	@if(app()->getLocale() === 'en')
                  <strong>{{ $subject->en_name }}</strong>
                @else
                  <strong>{{ $subject->name }}</strong>
                @endif
            </h2>
            
          </div>
        
    <!-- 內容 -->
    <div class="content">
      <div class="row">
        <div class="col-12 col-md-9 order-md-2">
          <div class="company-title">
            <h4 class="sr-only">{{ $contact->title }}</h4>
            <img src="{{ $contact->logo }}" alt="">
          </div>
          <div class="company-info">
            <div>
              <i class="fa fa-phone-alt"></i>：
              <a href="tel:{{ preg_replace('/[^\d\#]/','',$contact->tel) }}" title="">
                {{ $contact->tel }}
              </a>
            </div>
            <div>
              <i class="fa fa-fax"></i>：
              {{ $contact->fax }}
            </div>
            <div>
              <i class="fa fa-envelope"></i>：
              <a href="mailto:{{ $contact->email }}">
                {{ $contact->email }}
              </a>
            </div>
            <div>
              <!-- ↓圖文編輯器↓ -->
              <p>
                @if(app()->getLocale() === 'en')
                    {!! $contact->en_content !!}
                @else
                    {!! $contact->content !!}
                @endif    
              </p>
              <!-- ↑圖文編輯器↑ -->
            </div>
          </div>
        </div>
        <div class="col-12 order-md-2">
          <div class="map">
            {!! $contact->map !!}
          </div>
        </div>
        <div class="col-12 col-md-3 order-md-1">
          <img src="{{ $contact->src }}" class="w-100" alt="">
        </div>
      </div>
    </div>
  </div>    
</content>

@endsection