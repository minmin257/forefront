@extends('layout.default')
@section('banner')

  <div class="banner">
    <img src="{{ $sub->banner }}" alt="">        
  </div>

@endsection
@section('content')
      <content>
        <div class="container">
           <!-- 頁面標題 -->
          <div class="title">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">
                  @if(app()->getLocale() === 'en')
                      Home
                  @else
                      首頁
                  @endif
                </a></li>
                <li class="breadcrumb-item">
                  @if(app()->getLocale() === 'en')
                      Products
                  @else
                      產品與服務
                  @endif
                </li>
                <li class="breadcrumb-item"><a href="{{ route('page',['subject'=> $subject->en_name , 'sub'=> $sub->id]) }}">
                  @if(app()->getLocale() === 'en')
                      {{ $sub->en_name }}
                  @else
                      {{ $sub->name }}
                  @endif
                </a></li>
              </ol>
            </nav>
            <h2>
              @if(app()->getLocale() === 'en')
                  <strong>{{ $subject->en_name }}</strong>
                @else
                  <strong>{{ $subject->name }}</strong>
                @endif
            </h2>
            <h3>
              @if(app()->getLocale() === 'en')
                  <strong>{{ $sub->en_name }}</strong>
                @else
                  <strong>{{ $sub->name }}</strong>
                @endif
            </h3>
          </div>
          <!-- 內容 -->
          <div class="content">
            <!-- 搜尋 -->
            <div class="row justify-content-center mb-4">
              <div class="col-12 col-md-8 col-lg-6">
                <form action="{{ route('search') }}" method="GET">
                  <div class="input-group">
                    <input type="hidden" name="sub" value="{{ $sub->id }}">
                    <input type="hidden" name="subject" value="{{ $subject->id }}">
                    @if(app()->getLocale() === 'en')
                      <input name="search" type="text" class="form-control" value="{{ Request::get('search') }}" aria-describedby="button-addon2">
                    @else
                       <input name="search" type="text" class="form-control" value="{{ Request::get('search') }}" aria-describedby="button-addon2">
                    @endif
                    <div class="input-group-append">
                      <button class="btn btn-006300" type="submit" id="button-addon2">
                        <i class="fa fa-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- 產品列表 -->
            @if (isset($products) && (count($products)!=0))
            <table class="table table-borderless table-striped table-hover">
              <tbody>
                @foreach ($products as $product)
                <tr class="product-item">
                  <td>
                    <span class="p-icon"></span>
                  </td>
                  <td>
                    {{ $product->en_name }}
                  </td>
                  <td>
                    @if (app()->getLocale() === 'zh_TW')
                      {{ $product->name }}
                    @endif
                      
                  </td>
                  <td>
                    @if (!is_null($product->src))
                      <a href="{{ $product->src }}" data-lightbox="product" title="{{ $product->en_name }}">
                      <i class="fa fa-picture-o"></i>
                    </a>
                    @endif                    
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            @else
              <div class="row justify-content-center mb-4">
                @if (app()->getLocale() === 'zh_TW')
                  <span>
                    無搜尋結果，請更換關鍵字重新搜尋。
                  </span>
                @else
                  <span>
                    No search results, please change keywords and search again.
                  </span>
                @endif
              </div>
            @endif
          </div>
        </div>
      </content>
      
@endsection