@extends('layout.default')
@section('banner')

  <div class="banner">
    <img src="{{ $sub->banner }}" alt="">        
  </div>

@endsection
@section('content')
<content>
        <div class="container">
           <!-- 頁面標題 -->
          <div class="title">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">
                	@if(app()->getLocale() === 'en')
                  		Home
                	@else
                  		首頁
                	@endif
                </a></li>
                <li class="breadcrumb-item">
                	@if(app()->getLocale() === 'en')
                  		{{ $subject->en_name }}
                	@else
                  		{{ $subject->name }}
                	@endif
                </li>
                <li class="breadcrumb-item"><a href="{{ route('page', ['subject'=> $subject->en_name , 'sub'=> $sub->en_name]) }}">
                	@if(app()->getLocale() === 'en')
                  		{{ $sub->en_name }}
                	@else
                  		{{ $sub->name }}
                	@endif
                </a></li>
              </ol>
            </nav>
            <h2>
            	@if(app()->getLocale() === 'en')
                  <strong>{{ $subject->en_name }}</strong>
                @else
                  <strong>{{ $subject->name }}</strong>
                @endif
            </h2>
            <h3>
              @if(app()->getLocale() === 'en')
                  <strong>{{ $sub->en_name }}</strong>
                @else
                  <strong>{{ $sub->name }}</strong>
                @endif
            </h3>
          </div>
          <!-- 內容 -->
          <div class="content">
            <div>
            <!-- ↓圖文編輯器↓ -->
              
            @if (isset($about))
		        @if(app()->getLocale() === 'en')
		           {!! $about->en_content !!}
		        @else
		           {!! $about->content !!}
		        @endif
            @endif
            
            <!-- ↑圖文編輯器↑ -->
            </div>

            <ul class="history">
            @if(isset($histories))
            @foreach ($histories as $history)
            				
              <li class="history-item wow fadeInUp">
                <span class="date">
                <div>
                  {{ $history->year }}
            	</div>
                </span>
                <span class="text">
                  @if(app()->getLocale() === 'en')
                  	{!! $history->en_content !!}
                  @else
                  	{!! $history->content !!}
                  @endif
                </span>
              </li>

			@endforeach
   			@endif
            </ul>
          </div>
        </div>
      </content>
      
    </div>

@endsection