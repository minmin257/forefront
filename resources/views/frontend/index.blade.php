@extends('layout.default')
@section('banner')
<div class="banner">
        <div id="carouselIndex" class="carousel slide" data-ride="carousel">
        @if (isset($banners))
          <ol class="carousel-indicators">
            @if(app()->getLocale() === 'en')
              @foreach ($en_banners as $key => $en_banner)
              <li data-target="#carouselIndex" data-slide-to="{{ $key }}" class="{{ ($key == 0)?'active':null }}"></li>
              @endforeach
            @else
              @foreach ($banners as $key => $banner)
              <li data-target="#carouselIndex" data-slide-to="{{ $key }}" class="{{ ($key == 0)?'active':null }}"></li>
              @endforeach
            @endif
          </ol>
          <div class="carousel-inner">
            @if(app()->getLocale() === 'en')
              @foreach ($en_banners as $key => $en_banner)
                <div class="carousel-item {{ ($key == 0)?'active':null }}">
                <a href="{{ $en_banner->en_link }}"> 
                <img src="{{ $en_banner->en_src }}" class="d-block w-100">
                </a>
                </div>
              @endforeach
            @else
                @foreach ($banners as $key => $banner)
                  <div class="carousel-item {{ ($key == 0)?'active':null }}">
                  <a href="{{ $banner->link }}"> 
                  <img src="{{ $banner->src }}" class="d-block w-100">
                  </a>
                </div>
                @endforeach
            @endif

          @endif
          </div>
          <a class="carousel-control-prev" href="#carouselIndex" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselIndex" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        
      </div>
@endsection

@section('content')
<content>
  <div class="container">
    <div class="row justify-content-center mt-60 mb-40">
    @foreach ($quick_links as $quick_link)
      <div class="col-12 col-md-6 col-xl-4">
          @if(app()->getLocale() === 'en')
            <a class="index-product-item wow zoomIn" href="{{ $quick_link->en_link }}">
              <img src="{{ $quick_link->en_src }}" alt="">
              <div class="IPtitle">
                <span>{{ $quick_link->en_name }}</span>
              </div>
            </a>
          @else
            <a class="index-product-item wow zoomIn" href="{{ $quick_link->link }}">
              <img src="{{ $quick_link->src }}" alt="">
              <div class="IPtitle">
                <span>{{ $quick_link->name }}</span>
              </div>
            </a>  
          @endif
        
      </div>
    @endforeach
    </div>
  </div>
</content>
@endsection
    <!-- footer====================================================== -->
 