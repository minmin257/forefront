@inject('system', 'App\Repositories\SystemRepository')
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ $system->read()->sitename }}</title>
		<link rel="shortcut icon" href="/images/logo-icon.png" />
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ asset('css/backend/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/backend/bootstrap_r.css') }}">
		<link rel="stylesheet" href="{{ asset('font-awesome/all.css') }}">

		<link rel="stylesheet" href="{{ asset('css/backend/backend.css') }}">
	</head>

	<body>
		<!-- 上方登入狀態欄 -->
		@yield('navbar')

		<div class="main-container" id="main-container">
			<!-- 側邊功能欄 -->
			@yield('sidebar')

			<!-- 內容頁 -->
			@yield('content')

			<!-- 頁尾 -->
			@yield('footer')
		</div>
	</body>
	<script type="text/javascript" src="/js/tinymce/tinymce.js"></script>
    <script type="text/javascript" src="/js/tinymce.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="{{ asset('js/backend/back.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/backend/bootstrap.min.js') }}"></script>
	@yield('js')
</html>
