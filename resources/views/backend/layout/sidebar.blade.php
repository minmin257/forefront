<div id="sidebar" class="sidebar responsive" data-sidebar="true" data-sidebar-scroll="true" data-sidebar-hover="true">
	<ul class="nav nav-list">
		<li class="{{ active('backend.index') }}">
			<a href="{{ route('backend.index') }}">
				<i class="menu-icon fas fa-tachometer-alt"></i>
				<span class="menu-text">首頁</span>
			</a>
			<b class="arrow"></b>
		</li>

		<li class="{{ Request::segment(2) === 'content' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon far fa-edit"></i>
				<span class="menu-text">內容管理</span>
				<b class="arrow fas fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{ (Request::segment(3) === 'home_banner' || Request::segment(3) === 'quicklink') ? 'open' : '' }}">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						首頁管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu nav-hide">
						<li class="{{ active('backend.home_banner') }}">
							<a href="{{ route('backend.home_banner') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								首頁輪播
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ active('backend.quicklink') }}">
							<a href="{{ route('backend.quicklink') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								連結管理
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				<li class="{{ active('backend.inner_banner') }}">
					<a href="{{ route('backend.inner_banner') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						內頁banner管理
					</a>
					<b class="arrow"></b>
				</li>
                <li class="{{ (Request::segment(3) === 'about' || Request::segment(3) === 'history') ? 'open' : '' }}">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fas fa-caret-right"></i>
                        關於我們管理
                        <b class="arrow fas fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>

                    <ul class="submenu nav-hide">
                        <li class="{{ Request::segment(3) === 'about' ? 'active' : '' }}">
                            <a href="{{ route('backend.about') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                頁面管理
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="{{ Request::segment(3) === 'history' ? 'active' : '' }}">
                            <a href="{{ route('backend.history') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                公司沿革管理
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
				@if(auth()->user()->hasPermission('產品管理'))
				<li class="{{ (Request::segment(3) === 'productCategory' || Request::segment(3) === 'product' || Request::segment(3) === 'edit_product') ? 'open' : '' }}">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						產品資訊管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu nav-hide">
						<li class="{{ (Request::segment(3) === 'productCategory' || Request::segment(3) === 'edit_product') ? 'active' : '' }}">
							<a href="{{ route('backend.category') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								分類管理
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ Request::segment(3) === 'product' ? 'active' : '' }}">
							<a href="{{ route('backend.product') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								產品管理
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				@endif
				<li class="{{ Request::segment(3) === 'contact' ? 'active' : '' }}">
					<a href="{{ route('backend.contact') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						<span class="menu-text">聯絡我們管理</span>
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>

		<li class="{{  Request::segment(2) === 'authority' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fas fa-users-cog"></i>
				<span class="menu-text">權限管理</span>
				<b class="arrow fas fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{ active('backend.profile') }}">
					<a href="{{ route('backend.profile') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						基本資料
					</a>
					<b class="arrow"></b>
				</li>
				@if(auth()->user()->hasPermission('權限管理'))
				<li class="{{ (Request::segment(count(Request::segments())) === 'edit_user' || Request::segment(count(Request::segments())) === 'permission') ? 'active' : '' }}{{ active('backend.management') }}">
					<a href="{{ route('backend.management') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						帳號管理
					</a>
					<b class="arrow"></b>
				</li>
				@endif
			</ul>
		</li>
		@if(auth()->user()->hasPermission('系統設定'))
		<li class="{{ Request::segment(2) === 'system' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fas fa-cogs"></i>
				<span class="menu-text">系統設定</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				
				<li class="{{ active('backend.system') }}">
					<a href="{{ route('backend.system') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						一般設定
					</a>
					<b class="arrow"></b>
				</li>
				
			</ul>
		</li>
		@endif
	</ul>
	
	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fas fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fas fa-angle-double-right"></i>
	</div>
</div>
