@extends('backend.layout.default')
	@section('navbar')
		@include('backend.layout.navbar')
	@endsection

	@section('sidebar')
		@include('backend.layout.sidebar')
	@endsection

	@section('footer')
		@include('backend.layout.footer')
	@endsection
