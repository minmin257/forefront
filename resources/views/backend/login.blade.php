<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

		<title>登入頁</title>

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="/css/backend/bootstrap.min.css">
		<link rel="stylesheet" href="/font-awesome/all.css">

		<link rel="stylesheet" href="/css/backend/backend.css">
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	</head>

	<body style="background-image: url('/images/pattern.jpg')">
		<div class="container" style="margin: 100px auto;">
		    <div class="row vertical-offset-100">
		    	<div class="col-sm-6 col-xs-12 col-sm-offset-3">
		    		<img src="/images/logo-lg.png" alt="">
		    	</div>
		    	
		    	<div class="col-sm-6 col-xs-12 col-sm-offset-3">
		    		<div class="panel">
					  	<div class="panel-heading">
					  		<h3 class="header">
					  			<i class="ace-icon far fa-keyboard green"></i> 後台登入頁
					  		</h3>
					 	</div>
					  	<div class="panel-body">
					    	<form method="post" action="{{ route('login') }}">
					    		@csrf
			                    <fieldset style="border: 0px;">
						    	  	<div class="form-group">
						    	  		<label>帳號：</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fas fa-user" aria-hidden="true"></i></span>
											<input type="text" class="form-control" name="account" placeholder="帳號" required>
										</div>
						    		</div>
						    		<div class="form-group">
						    			<label>密碼：</label>
						    			<div class="input-group">
						    				<span class="input-group-addon"><i class="fas fa-user-lock" aria-hidden="true"></i></span>
						    				<input type="password" class="form-control" name="password" placeholder="密碼" required>
						    			</div>
						    		</div>
									{!! NoCaptcha::display() !!}
						    		<div class="space"></div>
						    	    {{-- 錯誤警示位置 --}}
						    	    @include('errors.errors')
						    	    <button class="btn btn-lg btn-success btn-block">
						    	    	<i class="ace-icon fa fa-key"></i> 登入
						    	    </button>
						    	</fieldset>
					      	</form>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
