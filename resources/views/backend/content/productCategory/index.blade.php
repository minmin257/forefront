@extends('backend.content.productCategory.default')

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        分類管理
                    </h1>
                </div>
                @include('success')
                <div class="row">
                    <div class="col-xs-12">
                        <a href="#" class="btn btn-default" role="button" data-toggle="modal"
                           data-target="#myModal">新增</a>
                        <input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>產品</th>
                                    <th>刪除</th>
                                    <th>中文類別名稱</th>
                                    <th>英文類別名稱(頁面)</th>
                                    <th>英文類別名稱(選單)</th>
                                    <th>順序</th>
                                    <th>啟用狀態</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <form id="form" method="post" action="{{ route('update_productCategory') }}">
                                    @csrf
                                    @foreach($productCategories as $productCategory)
                                        <input type="hidden" name="id[]" value="{{ $productCategory->id }}">
                                        <tr>
                                            <td data-title="產品">
                                                    <a href="{{ route('edit_product', $productCategory->id) }}" class="btn btn-warning btn-outline btn-circle">
                                                        <i class="fas fa-bars"></i>
                                                    </a>
                                                </td>
                                            <td data-title="刪除">
                                                <button class="btn btn-danger btn-outline btn-circle" type="button"
                                                        onclick="alert_del('{{ $productCategory->id }}')">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                            </td>

                                            <td data-title="中文類別名稱">
                                                <input type="text" class="form-control" name="name[]" value="{{ $productCategory->name }}">
                                            </td>
                                            <td data-title="英文類別名稱(頁面)">
                                                <input type="text" class="form-control" name="fullname[]" value="{{ $productCategory->fullname }}">
                                            </td>
                                            <td data-title="英文類別名稱(選單)">
                                                <input type="text" class="form-control" name="en_name[]" value="{{ $productCategory->en_name }}">
                                            </td>
                                            <td data-title="順序">
                                                <input type="number" class="form-control" name="sort[]" pattern="[0-9]" value="{{ $productCategory->sort }}">
                                            </td>
                                            <td data-title="啟用狀態">
                                                <select class="form-control" name="state[]">
                                                    @if($productCategory->state)
                                                        <option value="1" selected>啟用</option>
                                                        <option value="0">關閉</option>
                                                    @else
                                                        <option value="1">啟用</option>
                                                        <option value="0" selected>關閉</option>
                                                    @endif
                                                </select>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- 新增區塊 -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">新增產品分類</h4>
                </div>
                <div class="modal-body">
                    <form id="form_create">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>中文類別名稱</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label>英文類別名稱(頁面)</label>
                                        <input type="text" class="form-control" name="fullname">
                                    </div>
                                    <div class="form-group">
                                        <label>英文類別名稱(選單)</label>
                                        <input type="text" class="form-control" name="en_name">
                                    </div>
                                    <div class="form-group">
                                        <label>順序</label>
                                        <input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
                                    </div>
                                    {{-- 錯誤警示位置 --}}
                                    <div id="error_message"></div>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    {{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
    {{-- <script type="text/javascript" src="/js/tinymce.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="/js/ajax.js"></script>
    <script type="text/javascript">
        $(document).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });

        function store() {
            tinyMCE.triggerSave();
            var ajaxRequest = new ajaxCreate('POST', '{{ route('create_productCategory') }}', $('#form_create').serialize());
            ajaxRequest.request();
        }

        function alert_del(id) {
            var ajaxRequest = new ajaxDel('POST', '{{ route('delete_productCategory') }}', id);
            ajaxRequest.request();
        }
    </script>
@endsection
