@extends('backend.content.quicklink.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							連結管理
						</h1>
					</div>
					@include('success')
					<div class="row">
						<div class="col-xs-12">

							{{-- 中英文切換 改成js連到controller 同nav --}}
							<div style="margin-bottom: 10px">
								@if (app()->getLocale() === 'zh-TW')
									<a href="{{ route('set_cookie', ['lang'=>'zh-TW']) }}" class="btn btn-info">中文</a>
                            		<a href="{{ route('set_cookie', ['lang'=>'en']) }}" class="btn btn-default">English</a>
								@else
									<a href="{{ route('set_cookie', ['lang'=>'zh-TW']) }}" class="btn btn-default">中文</a>
                            		<a href="{{ route('set_cookie', ['lang'=>'en']) }}" class="btn btn-info">English</a>
								@endif
                                
                            </div>

							<input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
							<div class="space"></div>
						</div>
						<div class="col-xs-12">
							<div class="table-responsive text-nowrap">
								<table class="table" style="table-layout: fixed;">
									<thead>
										<tr>
											<th>標題</th>
											<th>圖片<span style="color: red;">(700 * 400px)</span></th>
											<th>連結</th>
										</tr>
									</thead>

									<tbody>
										<form id="form" method="post" action="{{ route('update_quicklink') }}">
											@csrf
											@foreach($Quicklinks as $key => $Quicklink)
												
													<input type="hidden" name="id[]" value="{{ $Quicklink->id }}">
													<input type="hidden" id="thumbnail{{$key}}" name="filepath[]" value="{{ $Quicklink->src }}">
													<input type="hidden" id="en_thumbnail{{$key}}" name="en_filepath[]" value="{{ $Quicklink->en_src }}">

													<tr>
														<td data-title="標題">
															@if(app()->getLocale() === 'zh-TW')
																<input type="text" class="form-control" name=name[] value="{{ $Quicklink->name }}">
															@else
																<input type="text" class="form-control" name=en_name[] value="{{ $Quicklink->en_name }}">
															@endif		
														</td>

														<td data-title="圖片">
															@if(app()->getLocale() === 'zh-TW')
																<img id="holder{{$key}}" class="img-responsive img-shadow lfm" data-input="thumbnail{{$key}}" data-preview="holder{{$key}}" src="{{ $Quicklink->src }}">
															@else
																<img id="holder{{$key}}" class="img-responsive img-shadow lfm" data-input="en_thumbnail{{$key}}" data-preview="holder{{$key}}" src="{{ $Quicklink->en_src }}">
															@endif
														</td>
														<td data-title="連結">
															@if(app()->getLocale() === 'zh-TW')
																<input type="text" class="form-control" name=link[] value="{{ $Quicklink->link }}">
															@else
																<input type="text" class="form-control" name=en_link[] value="{{ $Quicklink->en_link }}">
															@endif		
														</td>
													</tr>	
											@endforeach
										</form>
										
										{{-- 錯誤警示位置 --}}
				    	    			@include('errors.errors')
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script src="/js/ajax.js"></script>

	<script type="text/javascript">
		$(function() {
			$('.lfm').filemanager('image');
		});

		function do_update(id)
		{
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_quicklink') }}',$('#form'+id).serialize());
			ajaxRequest.request();
		}

	</script>
	@endsection
