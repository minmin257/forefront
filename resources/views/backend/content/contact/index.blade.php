@extends('backend.content.contact.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            聯絡我們管理
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
                            <div class="space"></div>
                        </div>

                        <form id="form" method="post" action="{{ route('update_contact') }}">
                            @csrf
                            <div class="col-xs-12 col-md-6">
                                <input type="hidden" class="form-control" name="id" value="{{ $contact->id }}">
                                <div class="form-group">
                                    <label>標題</label>
                                        <input type="text" class="form-control" name="title" value="{{ $contact->title }}">
                                </div>
                                <div class="form-group">
                                    <label>示意圖<span style="color: red;">(圖片尺寸：600 * 400px)</span></label>
                                        <img id="src_holder{{$contact->id}}" class="img-responsive img-shadow lfm" data-input="src_thumbnail{{$contact->id}}" data-preview="src_holder{{$contact->id}}" src="{{ $contact->src }}">
                                        <input id="src_thumbnail{{$contact->id}}" class="form-control" type="hidden" name="src_filepath" value="{{ $contact->src }}">
                                </div>
                                <div class="form-group">
                                    <label>logo<span style="color: red;">(圖片尺寸：500 * 100px)</span></label>
                                        <img id="holder{{$contact->id}}" class="img-responsive img-shadow lfm" data-input="thumbnail{{$contact->id}}" data-preview="holder{{$contact->id}}" src="{{ $contact->logo }}">
                                        <input id="thumbnail{{$contact->id}}" class="form-control" type="hidden" name="filepath" value="{{ $contact->logo }}">
                                </div>
                                
                                <div class="form-group">
                                    <label>電話</label>
                                    <input type="text" class="form-control" name="tel" value="{{ $contact->tel }}">
                                </div>
                                <div class="form-group">
                                    <label>傳真</label>
                                    <input type="text" class="form-control" name="fax" value="{{ $contact->fax }}">
                                </div>
                                <div class="form-group">
                                    <label>公司信箱</label>
                                    <input type="text" class="form-control" name="email" value="{{ $contact->email }}">
                                </div>
                                <div class="form-group">
                                    <label>GoogleMap</label>
                                    <textarea class="form-control" name="map" rows="5">{{ $contact->map }}</textarea>
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-md-6">
                                
                                <div class="form-group">
                                    <label>中文內容</label>
                                    <textarea name="content" class="form-control editor">{!! $contact->content !!}</textarea>
                                    <div class="space"></div>
                                    <label>英文內容</label>
                                    <textarea name="en_content" class="form-control editor">{!! $contact->en_content !!}</textarea>
                                </div>
                            </div>
                        </form>

                        <div class="col-md-12">
                        {{-- 錯誤警示位置 --}}
                        @include('errors.errors')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('js')
    {{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
    {{-- <script type="text/javascript" src="/js/tinymce.js"></script> --}}
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script src="/js/ajax.js"></script>
    <script type="text/javascript">
        $(function() {
            $('.lfm').filemanager('image');
        });

    </script>
@endsection

