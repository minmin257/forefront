@extends('backend.content.about.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            {{ $about->name }}
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            {{-- 中英文切換 改成js連到controller 同nav --}}
                            <div style="margin-bottom: 10px">
                                @if (app()->getLocale() === 'zh-TW')
                                    <a href="{{ route('set_cookie', ['lang'=>'zh-TW']) }}" class="btn btn-info">中文</a>
                                    <a href="{{ route('set_cookie', ['lang'=>'en']) }}" class="btn btn-default">English</a>
                                @else
                                    <a href="{{ route('set_cookie', ['lang'=>'zh-TW']) }}" class="btn btn-default">中文</a>
                                    <a href="{{ route('set_cookie', ['lang'=>'en']) }}" class="btn btn-info">English</a>
                                @endif
                            </div>
                            
                            <div class="space"></div>
                            <a href="{{ route('backend.about') }}" class="btn btn-default">返回</a>
                            <div class="space"></div>

                            <form id="form" method="post" action="{{ route('update_about_detail', $about->id) }}">
                                @csrf
                                <input type="hidden" name="sub_id" value="{{ $about->sub_id }}">
                                @if(app()->getLocale() === 'zh-TW')
                                    <div class="form-group">
                                        <label>標題</label>
                                        <input type="text" name="title" class="form-control" value="{{ $about->sub->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label>順序</label>
                                        <input type="number" class="form-control" name="sort" pattern="[0-9]" value="{{ $about->sub->sort }}">
                                    </div>
                                    <div class="form-group">
                                        <label>內容</label>
                                        <textarea name="content" class="form-control editor">{!! $about->content !!}</textarea>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label>標題</label>
                                        <input type="text" name="en_title" class="form-control" value="{{ $about->sub->en_name }}">
                                    </div>
                                    <div class="form-group">
                                        <label>順序</label>
                                        <input type="number" class="form-control" name="sort" pattern="[0-9]" value="{{ $about->sub->sort }}">
                                    </div>
                                    <div class="form-group">
                                        <label>內容</label>
                                        <textarea name="en_content" class="form-control editor">{!! $about->en_content !!}</textarea>
                                    </div>
                                @endif
                            </form>
                            {{-- 錯誤警示位置 --}}
                            @include('errors.errors')
                            <input type="button" class="btn btn-primary" value="確定送出" onclick="$('#form').submit()">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('js')
        {{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
        {{-- <script type="text/javascript" src="/js/tinymce.js"></script> --}}
    @endsection
