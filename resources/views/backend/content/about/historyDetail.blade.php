@extends('backend.content.about.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            公司沿革 - {{ $History->year }}
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <div style="margin-bottom: 10px">
                                @if (app()->getLocale() === 'zh-TW')
                                    <a href="{{ route('set_cookie', ['lang'=>'zh-TW']) }}" class="btn btn-info">中文</a>
                                    <a href="{{ route('set_cookie', ['lang'=>'en']) }}" class="btn btn-default">English</a>
                                @else
                                    <a href="{{ route('set_cookie', ['lang'=>'zh-TW']) }}" class="btn btn-default">中文</a>
                                    <a href="{{ route('set_cookie', ['lang'=>'en']) }}" class="btn btn-info">English</a>
                                @endif
                            </div>

                            <div class="space"></div>
                            <a href="{{ route('backend.history') }}" class="btn btn-default">返回</a>
                            <div class="space"></div>
                            <div class="space"></div>

                            <form id="form" method="post" action="{{ route('update_history_detail', $History->id) }}">
                                @csrf
                                <input type="hidden" name="id" value="{{ $History->id }}">
                                @if(app()->getLocale() === 'zh-TW')
                                    <div class="form-group">
                                        <label>年份</label>
                                        <input type="number" class="form-control" name="year" pattern="[0-9]" value="{{ $History->year }}">
                                    </div>
                                    <div class="form-group">
                                        <label>內容</label>
                                        <textarea name="content" class="form-control editor">{!! $History->content !!}</textarea>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label>年份</label>
                                        <input type="number" class="form-control" name="year" pattern="[0-9]" value="{{ $History->year }}">
                                    </div>
                                    <div class="form-group">
                                        <label>內容</label>
                                        <textarea name="en_content" class="form-control editor">{!! $History->en_content !!}</textarea>
                                    </div>
                                @endif
                            </form>
                            {{-- 錯誤警示位置 --}}
                            @include('errors.errors')
                            <input type="button" class="btn btn-primary" value="確定送出" onclick="$('#form').submit()">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('js')
        {{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
        {{-- <script type="text/javascript" src="/js/tinymce.js"></script> --}}
    @endsection
