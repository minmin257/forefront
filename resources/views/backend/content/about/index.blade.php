@extends('backend.content.about.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            關於我們 - 頁面管理
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
                            <input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
                            <div class="space"></div>
                        </div>

                        <div class="col-xs-12">
                            <div class="table-responsive text-nowrap">
                                <table class="table" style="table-layout: fixed;">
                                    <thead>
                                        <tr>
                                            <th>編輯</th>
                                            <th>刪除</th>
                                            <th>標題</th>
                                            <th>英文標題</th>
                                            <th>順序</th>
                                            <th>啟用狀態</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <form id="form" method="post" action="{{ route('update_about') }}">
                                            @csrf 
                                        @foreach($Abouts as $key => $about)
                                        <tr>
                                             <td data-title="編輯"> 
                                                <a href="{{ route('edit_about', $about->sub_id) }}" class="btn btn-info btn-outline btn-circle">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            </td>
                                            <td data-title="刪除">
                                            @if ($about->sub_id != 2)
                                                <button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $about->sub_id }}')">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>           
                                            @endif
                                            </td>
                                            <td data-title="標題">
                                                {{ $about->name }}
                                            </td>
                                            <td data-title="英文標題">
                                                {{ $about->en_name }}
                                            </td>
                                            <td data-title="順序">
                                                {{ $about->sort }}
                                            </td>
                                            <td data-title="啟用狀態">
                                                <select class="form-control" name="state[{{ $about->sub_id }}]">
                                                    @if($about->state)
                                                    <option value="1" selected>啟用</option>
                                                    <option value="0">關閉</option>
                                                    @else
                                                    <option value="1">啟用</option>
                                                    <option value="0" selected>關閉</option>
                                                    @endif
                                                </select>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </form>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 新增區塊 -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">新增頁面</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_create">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>中文標題</label>
                                            <input type="text" name="name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>中文內容</label>
                                            <textarea name="content" class="form-control editor"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>英文標題</label>
                                            <input type="text" name="en_name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>英文內容</label>
                                            <textarea name="en_content" class="form-control editor"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>順序</label>
                                            <input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
                                        </div>
                                        <div id="error_message"></div>
                                    </div>
                                    <!-- /.col-lg-12 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('js')
        {{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
        {{-- <script type="text/javascript" src="/js/tinymce.js"></script> --}}
        <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="/js/ajax.js"></script>
        <script type="text/javascript">
           

            $(function() {
                $('.lfm').filemanager('image');

                $(document).on('focusin', function(e) {
                    if ($(e.target).closest(".mce-window").length) {
                        e.stopImmediatePropagation();
                    }
                });
            });

            function store()
            {
                tinyMCE.triggerSave();
                var ajaxRequest = new ajaxCreate('POST','{{ route('create_about') }}',$('#form_create').serialize());
                ajaxRequest.request();

            }

            function alert_del(id)
            {
                console.log(id);
                var ajaxRequest = new ajaxDel('POST','{{ route('delete_about') }}',id);
                ajaxRequest.request();
            }
        </script>
    @endsection
