@extends('backend.content.homebanner.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							首頁輪播
						</h1>
					</div>
					@include('success')
					<div class="row">
						<div class="col-xs-12">
							{{-- 中英文切換 改成js連到controller 同nav --}}
							<div style="margin-bottom: 10px">
								@if (app()->getLocale() === 'zh-TW')
									<a href="{{ route('set_cookie', ['lang'=>'zh-TW']) }}" class="btn btn-info">中文</a>
                            		<a href="{{ route('set_cookie', ['lang'=>'en']) }}" class="btn btn-default">English</a>
								@else
									<a href="{{ route('set_cookie', ['lang'=>'zh-TW']) }}" class="btn btn-default">中文</a>
                            		<a href="{{ route('set_cookie', ['lang'=>'en']) }}" class="btn btn-info">English</a>
								@endif

							<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
							<input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
							<div class="space"></div>
						</div>
						<div class="col-xs-12">
							<div class="table-responsive text-nowrap">
								<table class="table" style="table-layout: fixed;">
									<thead>
										<tr>
											<th>刪除</th>
											<th>圖片<span style="color: red;">(1920 * 600px)</span></th>
											<th>連結</th>
											<th>順序</th>
											<th>啟用狀態</th>
										</tr>
									</thead>

									<tbody>
										<form id="form" method="post" action="{{ route('update_home_banner') }}">
											@csrf
											@if(app()->getLocale() === 'zh-TW')
												@foreach($Banners as $key => $Banner)
														<input type="hidden" name="id[]" value="{{ $Banner->id }}">
														<input type="hidden" id="thumbnail{{$key}}" name="filepath[]" value="{{ $Banner->src }}">
														<tr>
															<td data-title="刪除">
																<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $Banner->id }}')">
																	<i class="far fa-trash-alt"></i>
																</button>
															</td>
															<td data-title="圖片">
																<img id="holder{{$key}}" class="img-responsive img-shadow lfm" data-input="thumbnail{{$key}}" data-preview="holder{{$key}}" src="{{ $Banner->src }}">
															</td>
															<td data-title="連結">
																<input type="text" class="form-control" name=link[] value="{{ $Banner->link }}">	
															</td>
															<td data-title="順序">
																<input type="number" class="form-control" name="sort[]" pattern="[0-9]" value="{{ $Banner->sort }}">
															</td>
			                                                <td data-title="啟用狀態">
			                                                    <select class="form-control" name="state[]">
			                                                        @if($Banner->state)
			                                                        <option value="1" selected>啟用</option>
			                                                        <option value="0">關閉</option>
			                                                        @else
			                                                        <option value="1">啟用</option>
			                                                        <option value="0" selected>關閉</option>
			                                                        @endif
			                                                    </select>
			                                                </td>
														</tr>	
												@endforeach
											@else
												@foreach($en_Banners as $key => $en_Banner)
														<input type="hidden" name="id[]" value="{{ $en_Banner->id }}">
														<input type="hidden" id="en_thumbnail{{$key}}" name="en_filepath[]" value="{{ $en_Banner->en_src }}">

														<tr>
															<td data-title="刪除">
																<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $en_Banner->id }}')">
																	<i class="far fa-trash-alt"></i>
																</button>
															</td>
															<td data-title="圖片">
																<img id="holder{{$key}}" class="img-responsive img-shadow lfm" data-input="en_thumbnail{{$key}}" data-preview="holder{{$key}}" src="{{ $en_Banner->en_src }}">
															</td>
															<td data-title="連結">
																<input type="text" class="form-control" name=en_link[] value="{{ $en_Banner->en_link }}">	
															</td>
															<td data-title="順序">
																<input type="number" class="form-control" name="en_sort[]" pattern="[0-9]" value="{{ $en_Banner->en_sort }}">
															</td>
			                                                <td data-title="啟用狀態">
			                                                    <select class="form-control" name="en_state[]">
			                                                        @if($en_Banner->en_state)
			                                                        <option value="1" selected>啟用</option>
			                                                        <option value="0">關閉</option>
			                                                        @else
			                                                        <option value="1">啟用</option>
			                                                        <option value="0" selected>關閉</option>
			                                                        @endif
			                                                    </select>
			                                                </td>
														</tr>	
												@endforeach
											@endif		
										</form>
										{{-- 錯誤警示位置 --}}
				    	    			@include('errors.errors')
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增輪播</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									@if(app()->getLocale() === 'zh-TW')
									<div class="col-lg-12">
										<div class="form-group">
											<label>中文介面圖片<span style="color: red;">(圖片尺寸：1920 * 550px)</span></label>
											<button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
												<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="thumbnail" class="form-control" type="hidden" name="filepath">
										</div>
										<div class="form-group">
											<img id="holder" class="img-responsive">
										</div>
										<div class="form-group">
											<label>連結</label>
											<input type="text" class="form-control" name="link">
										</div>
										<div class="form-group">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
										</div>
										<div id="error_message"></div>
									</div>
									@else
									<div class="col-lg-12">
										<div class="form-group">
											<label>英文介面圖片<span style="color: red;">(圖片尺寸：1920 * 550px)</span></label>
											<button class="form-control btn btn-primary lfm" data-input="en_thumbnail" data-preview="en_holder">
												<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="en_thumbnail" class="form-control" type="hidden" name="en_filepath">
										</div>
										<div class="form-group">
											<img id="en_holder" class="img-responsive">
										</div>
										<div class="form-group">
											<label>連結</label>
											<input type="text" class="form-control" name="en_link">
										</div>

										<div class="form-group">
											<label>順序</label>
											<input type="number" class="form-control" name="en_sort" pattern="[0-9]" value="0">
										</div>
										<div id="error_message"></div>
									</div>
									@endif
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script src="/js/ajax.js"></script>

	<script type="text/javascript">
		function language(id){
		          console.log(id)

		          $('input[name="lang"]').val(id)
		          $('form[id="lang"]').submit();
		      }

		$(function() {
			$('.lfm').filemanager('image');
		});

		function store()
		{
			var ajaxRequest = new ajaxCreate('POST','{{ route('create_home_banner') }}',$('#form_create').serialize());
			ajaxRequest.request();
		}

		function alert_del(id)
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_home_banner') }}',id);
			ajaxRequest.request();
		}
	</script>
	@endsection
