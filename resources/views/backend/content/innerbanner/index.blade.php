@extends('backend.content.innerbanner.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							內頁banner
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-7">
							<ul class="nav nav-pills" id="pills-tab" role="tablist">
								@foreach($subs as $sub)
								<li class="nav-item">
									<a class="nav-link" data-toggle="pill" href="#pills-{{$loop->iteration}}">{{ $sub->name }}</a>
								</li>
								@endforeach
							</ul>
							<div class="space"></div>

							<div class="tab-content">
								@foreach($subs as $sub)
								<div class="tab-pane fade" id="pills-{{$loop->iteration}}">
									<form id="form{{$loop->iteration}}">
										<input type="hidden" name="id" value="{{ $sub->id }}">
										<label style="color: #ccc">點兩下圖片刪除</label><br>
										<span style="color: red;">(圖片尺寸：1920 * 320px)
										<button class="form-control btn btn-primary lfm" data-input="thumbnail{{$sub->id}}" data-preview="holder{{$sub->id}}">
											<i class="far fa-image"></i>上傳圖片</span>
										</button>
										<input id="thumbnail{{$sub->id}}" class="form-control" type="hidden" name="filepath" value="{{ $sub->banner }}">
										<div class="form-group">
											<img id="holder{{$sub->id}}" class="img-responsive img-shadow" src="{{ $sub->banner }}" ondblclick="alert_del('{{$sub->id}}')">
										</div>
									</form>
									<input type="button" class="btn btn-primary float-right" value="確定送出" onclick="do_update({{$sub->id}})">
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		$(function() {
			$('.lfm').filemanager('image');
		});

		function do_update(id)
		{
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_inner_banner') }}',$('#form'+id).serialize());
			ajaxRequest.request();
		}

		function alert_del(id)
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_inner_banner') }}',id);
			ajaxRequest.request();
		}
	</script>
	@endsection
